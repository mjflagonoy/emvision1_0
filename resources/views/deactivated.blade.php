@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card" >
        <div class="card-body">
            Your account has been deactivated.
        </div>
      </div>
</div>
@endsection