@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY=" crossorigin="anonymous" />

<div class="container">
            
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    
                    <div class="career-search mb-60">
                        

                        <div class="filter-result"><br>
                            <h4 class="text-secondary">My Active Job Application(s)</h4>
                            <hr>
                            @if($posts[0] !== null)
                              @foreach($posts as $post)
                                  <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                                      <div class="job-left my-4 d-md-flex align-items-center flex-wrap">
                                          {{-- <div class="img-holder mr-md-4 mb-md-0 mb-4 mx-auto mx-md-0 d-md-none d-lg-flex">
                                              FD
                                          </div> --}}
                                          <div class="job-content">
                                              <a href="{{url('detail/'.Str::slug($post->post->title).'/'.$post->post->id)}}"><p class="text-center text-md-left">{{ $post->post->title }}</p></a>
                                              <ul class="d-md-flex flex-wrap text-capitalize ff-open-sans">
                                                  <li class="mr-md-5">
                                                      <i class="fa fa-check"></i> {{ $post->post->getStatus() }}
                                                  </li>
                                                
                                                  <li class="mr-md-4">
                                                      <i class="fa fa-file"></i> {{ $post->application_status }}
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>
                                      <div class="job-right my-4 flex-shrink-0">
                                          <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}"  class="btn d-block w-100 d-sm-inline-block btn-light">View</a>
                                      </div>
                                  </div>
                              @endforeach
                            @else 
                            <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                            @endif
                            
                            
                        </div>

                        
                    </div>
                    
                </div>
            </div>
            <!-- Pagination -->
            {{$posts->links('vendor.pagination.custompagination')}}

            <div class="filter-result"><br>
              <h4 class="text-secondary">My Closed Job Application(s)</h4>
              <hr>
              @if($inactive[0] !== null)
                @foreach($inactive as $post)
                    <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                        <div class="job-left my-4 d-md-flex align-items-center flex-wrap">
                            
                            <div class="job-content">
                                <a href="{{url('detail/'.Str::slug($post->post->title).'/'.$post->post->id)}}"><p class="text-center text-md-left">{{ $post->post->title }}</p></a>
                                <ul class="d-md-flex flex-wrap text-capitalize ff-open-sans">
                                    <li class="mr-md-5">
                                        <i class="fa fa-times"></i> Closed
                                    </li>
                                  
                                    <li class="mr-md-4">
                                      <i class="fa fa-file"></i> {{ $post->application_status }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="job-right my-4 flex-shrink-0">
                            <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}"  class="btn d-block w-100 d-sm-inline-block btn-light">View</a>
                        </div>
                    </div>
                @endforeach
                 <!-- Pagination -->
                 {{$inactive->links('vendor.pagination.custompagination')}}
              @else
              <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
              @endif
              
             
              
          </div>
        </div>

        
       

        <style>


/* ===== Career ===== */
.career-form {
  background-color: #f5f5f5;
  border-radius: 5px;
  padding: 0 16px;
}

.career-form .form-control {
  border: 0;
  padding: 12px 15px;

}

.career-form .custom-select {
  border: 0;
  padding: 12px 15px;
  width: 100%;
  border-radius: 5px;
  text-align: left;
  height: auto;
  background-image: none;
}

.career-form .custom-select:focus {
  -webkit-box-shadow: none;
          box-shadow: none;
}

.career-form .select-container {
  position: relative;
}

.career-form .select-container:before {
  position: absolute;
  right: 15px;
  top: calc(50% - 14px);
  font-size: 18px;
  content: '\F2F9';
  font-family: "Material-Design-Iconic-Font";
}

.filter-result .job-box {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
  padding: 10px 35px;
}

ul {
  list-style: none; 
}

.list-disk li {
  list-style: none;
  margin-bottom: 12px;
}

.list-disk li:last-child {
  margin-bottom: 0;
}

.job-box .img-holder {
  height: 65px;
  width: 65px;
  background-color: #2A57A3;
  font-family: "Open Sans", sans-serif;
  color: #fff;
  font-size: 22px;
  font-weight: 700;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-radius: 65px;
}

.career-title {
  background-color: #2A57A3;
  color: #fff;
  padding: 15px;
  text-align: center;
  border-radius: 10px 10px 0 0;
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
  background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
}

.job-overview {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
}

@media (min-width: 992px) {
  .job-overview {
    position: -webkit-sticky;
    position: sticky;
    top: 70px;
  }
}

.job-overview .job-detail ul {
  margin-bottom: 28px;
}

.job-overview .job-detail ul li {
  opacity: 0.75;
  font-weight: 600;
  margin-bottom: 15px;
}

.job-overview .job-detail ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}

.job-overview .overview-bottom,
.job-overview .overview-top {
  padding: 35px;
}

.job-content ul li {
  border-bottom: 1px solid #ccc;
  padding: 10px 5px;
}

@media (min-width: 768px) {
  .job-content ul li {
    border-bottom: 0;
    padding: 0;
  }
}

.job-content ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}
.mb-30 {
    margin-bottom: 30px;
}
        </style>
@endsection