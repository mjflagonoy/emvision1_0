@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="search-result-box card-box">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="pt-3 pb-4">
                                
                                <div class="mt-4 text-center">
                                    <p>Search Result(s) For {{ '"'.$query.'"'}}</p></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="nav-item"><a href="#company" data-toggle="tab" aria-expanded="true" class="nav-link active">Company<span class="badge badge-success ml-1">{{ count($companies) }}</span></a></li>
                        <li class="nav-item"><a href="#jobseeker" data-toggle="tab" aria-expanded="false" class="nav-link">Job Seeker <span class="badge badge-danger ml-1">{{ count($applicants) }}</span></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="company">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(count($companies) > 0)
                                        @foreach ($companies as $company)
                                            <div class="search-item">
                                                <h4 class="mb-1"><a href="/{{$company->username}}">{{$company->name}}</a></h4>
                                                <div class="font-13 text-success mb-3">{{ $company->website }}</div>
                                                <p class="mb-0 text-muted">{{ $company->address }}</p>
                                            </div>
                                        @endforeach
                                        <!-- Pagination -->
                                        {{$companies->links('vendor.pagination.custompagination')}}
              
                                    @else 
                                        <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end All results tab -->
                        <!-- Users tab -->
                        <div class="tab-pane" id="jobseeker">
                            @if(count($applicants) > 0)
                                @foreach ($applicants as $applicant)
                                    <div class="search-item">
                                        <div class="media mt-1"><img class="d-flex mr-3 rounded-circle" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="Generic placeholder image" height="54">
                                            <div class="media-body">
                                                <h3 class="media-heading mt-0"><a href="/{{ $applicant->username }}" >{{ $applicant->name}}</a></h3>
                                                <p class="font-13"><b>Email:</b> <span><a href="#" class="text-muted">{{ $applicant->email }}</a></span></p>
                                                <p class="mb-0 font-13"><b>Bio:</b>
                                                    <br><span class="text-muted">{{ $applicant->biography }}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <!-- Pagination -->
                                {{$applicants->links('vendor.pagination.custompagination')}}
              
                            @else 
                                <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                            @endif

                            <div class="clearfix"></div>
                        </div>
                        <!-- end Users tab -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- container -->
</div>

<style>
    body{
    margin-top:20px;
    background:#dcdcdc;
}
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}
.search-result-box .tab-content {
    padding: 30px 30px 10px 30px;
    -webkit-box-shadow: none;
    box-shadow: none;
    -moz-box-shadow: none
}

.search-result-box .search-item {
    padding-bottom: 20px;
    border-bottom: 1px solid #e3eaef;
    margin-bottom: 20px
}
.text-success {
    color: #0acf97!important;
}
a {
    color: #007bff;
    text-decoration: none;
    background-color: transparent;
}
.btn-custom {
    background-color: #02c0ce;
    border-color: #02c0ce;
}

.btn-custom, .btn-danger, .btn-info, .btn-inverse, .btn-pink, .btn-primary, .btn-purple, .btn-success, .btn-warning {
    color: #fff!important;
}
</style>
@endsection