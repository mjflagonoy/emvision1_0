@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY=" crossorigin="anonymous" />

<div class="container">
            
            <div class="row m-0 p-0">
                <div class="col-lg-4">
                  <div class="card">
                    <div class="card-header p-3">
                      <h5>Search Bar</h5>
                    </div>
                    <div class="card-body">
                      <form action="{{url('jobs/search')}}" enctype="multipart/form-data" method="get">
                        <h5 class="text-secondary">Keyword</h5>
                        <input onclick='listen("search")' data-provide="typeahead"  type="text" name="searchKeyword" id="search" class="form-control mb-3" placeholder="Enter keyword"/>

                        <h5 class="text-secondary">Salary Range</h5>
                        <input onclick='listen("salaryMin")' id="salaryMin" type="number" class="form-control float-left" style="width: 45%" name="salaryMin" placeholder="Min">
                        <small class="text-secondary float-center">to</small>
                        <input onclick='listen("salaryMax")' id="salaryMax" type="number" class="form-control float-right" style="width: 45%" name="salaryMax" placeholder="Max">

                        <h5 class="mt-3 text-secondary">Category</h5>
                        <select class="form-control" name="searchCategory">
                          <option selected disabled>Search Category</option>
                          @foreach($cats as $cat)
                          <option value="{{$cat->id}}">{{$cat->title}}</option>
                          @endforeach
                        </select>

                        <input type="submit" value="Search" class="form-control btn btn-primary mt-3">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="col-lg-8 mx-auto m-0 p-0">
                    @if(auth()->user()->role === 'employer')
                        <div class="row">
                            <div class="col-md-6 col-lg-3 my-3">
                                <a href="{{ url('company/add-job') }}">
                                    <button type="button" class="btn btn-lg btn-block btn-light btn-custom" id="contact-submit">
                                        Add Job Post
                                    </button>
                                </a>
                            </div>
                        </div>
                    @endif

                    <div class="career-search mb-60">
                        

                        <div class="filter-result">
                            <h4 class="text-secondary">Total Job Openings ({{ $posts->count() }})</h4>
                            <hr>
                            @if(isset($posts))
                              @foreach($posts as $post)
                                  <div class="job-box d-md-flex align-items-center justify-content-between mb-30">
                                      <div class="job-left my-4 d-md-flex align-items-center flex-wrap">
                                          {{-- <div class="img-holder mr-md-4 mb-md-0 mb-4 mx-auto mx-md-0 d-md-none d-lg-flex">
                                              FD
                                          </div> --}}
                                          <div class="job-content">
                                              <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}"><p class="text-center text-md-left">{{ $post->title }}</p></a>
                                              <ul class="d-md-flex flex-wrap text-capitalize ff-open-sans">
                                                  <li class="mr-md-4">
                                                      <i class="zmdi zmdi-pin mr-2"></i> {{ $post->workCity }}
                                                  </li>
                                                  <li class="mr-md-4">
                                                      <small class="fa-solid fa-peso-sign">PhP</small> {{ $post->salaryAvg  }}
                                                  </li>
                                                  <li class="mr-md-4">
                                                      <i class="zmdi zmdi-time mr-2"></i> {{ $post->contract }}
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>
                                      <div class="job-right my-4 flex-shrink-0">
                                        <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}" class="btn d-block w-100 d-sm-inline-block btn-light">View</a>
                                      </div>
                                  </div>
                              @endforeach
                              <!-- Pagination -->
                              <br>
                              {{$posts->links('vendor.pagination.custompagination')}}
                            @else 
                              <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                            @endif
                            
                            
                        </div>
                    </div>
                    
                </div>
                
            </div>
            

        </div>
       
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js">
        </script>
        <script type="text/javascript">
            var route = "{{ url('auto-search') }}";
    
            $('#search').typeahead({
                source: function (query, process) {
                    return $.get(route, {
                        query: query
                    }, function (data) {
                        return process(data);
                    });
                }
            });
        </script>
<style>

/* ===== Career ===== */
.career-form {
  background-color: #fff;
  border-radius: 5px;
  padding: 0 16px;
}

.career-form .form-control {
  border: 0;
  padding: 12px 15px;

}

.career-form .custom-select {
  border: 0;
  padding: 12px 15px;
  width: 100%;
  border-radius: 5px;
  text-align: left;
  height: auto;
  background-image: none;
}

.career-form .custom-select:focus {
  -webkit-box-shadow: none;
          box-shadow: none;
}

.career-form .select-container {
  position: relative;
}

.career-form .select-container:before {
  position: absolute;
  right: 15px;
  top: calc(50% - 14px);
  font-size: 18px;
  content: '\F2F9';
  font-family: "Material-Design-Iconic-Font";
}

.filter-result .job-box {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
  padding: 10px 35px;
}

ul {
  list-style: none; 
}

.list-disk li {
  list-style: none;
  margin-bottom: 12px;
}

.list-disk li:last-child {
  margin-bottom: 0;
}

.job-box .img-holder {
  height: 65px;
  width: 65px;
  background-color: #2A57A3;
  font-family: "Open Sans", sans-serif;
  color: #fff;
  font-size: 22px;
  font-weight: 700;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-radius: 65px;
}

.career-title {
  background-color: #2A57A3;
  color: #fff;
  padding: 15px;
  text-align: center;
  border-radius: 10px 10px 0 0;
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
  background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
}

.job-overview {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
}

@media (min-width: 992px) {
  .job-overview {
    position: -webkit-sticky;
    position: sticky;
    top: 70px;
  }
}

.job-overview .job-detail ul {
  margin-bottom: 28px;
}

.job-overview .job-detail ul li {
  opacity: 0.75;
  font-weight: 600;
  margin-bottom: 15px;
}

.job-overview .job-detail ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}

.job-overview .overview-bottom,
.job-overview .overview-top {
  padding: 35px;
}

.job-content ul li {
  border-bottom: 1px solid #ccc;
  padding: 10px 5px;
}

@media (min-width: 768px) {
  .job-content ul li {
    border-bottom: 0;
    padding: 0;
  }
}

.job-content ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}

        </style>
@endsection