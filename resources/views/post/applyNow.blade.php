@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    Job Detail
                </div>
                <div class="card-body">
                    <a href="{{ url('detail/'.$detail->title.'/'.$detail->id) }}"><h4 class="font-weight-bold">{{ $detail->title }}</h4></a>
                    <h5>by <a href="{{ url($detail->user->username) }}">{{ $detail->user->name }}</a></h5>
                </div>
            </div>
        </div>

        <div class="col-lg-8"> 
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('jobs/apply/'.$detail->id.'/submit_application') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h5 class="font-weight-bold">Tell the employer why they should hire you.</h5>
                        <script>
                            CKEDITOR.replace('editor1', {
                            height: 150,
                            removeButtons: 'PasteFromWord'
                            });
                        </script>
                        <textarea onclick='listen("applicantReason")' id="applicantReason" class="ckeditor form-control" name="applicantReason" placeholder="Why should they hire you?" rows="5"></textarea>
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="chooseFile">
                            <label class="custom-file-label" for="chooseFile">Select Curriculum vitae/resume</label>
                        </div>
                        <input type="submit" value="Submit Application" class="mt-3 float-center btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.17.1/basic/ckeditor.js"></script>
@endsection