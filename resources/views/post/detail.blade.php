@extends('layouts.app')

@section('content')


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<div class="container p-0">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="{{ url('jobs') }}">All Job Ads</a>
        </li>
        <li class="breadcrumb-item active">Detail</li>
    </ol>
      <!-- /Breadcrumb -->

    <div class="row">
        
        <div class="col-md-5 col-xl-4">
            
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Job Details</h5>
                </div>

                <div class="list-group list-group-flush" role="tablist">
                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#overview" role="tab">
                      Job Overview
                    </a>
                    @if (auth()->user()->role === "applicant" and $job_application !== null)
                        @if($job_application->applicant_id === auth()->user()->id)
                            <a class="list-group-item list-group-item-action" data-toggle="list" href="#applicationStatus" role="tab">
                                Application Status
                            </a>
                        @endif 
                    @endif
                    
                    @if ($detail->user->id === auth()->user()->id)
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#applicants" role="tab">
                            Applicants <small><span class="badge badge-dark">{{count($job_application)}}</span></small>
                        </a>
                    @endif
                   
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#comments" role="tab">
                        Comments @if(auth()->user()->id === $detail->user->id) <small><span class="badge badge-dark">{{count($detail->comments)}}</span></small>@endif
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-7 col-xl-8">
            @if(Session::has('success'))
                <p class="text-success">{{session('success')}}</p>
            @endif
            @if(Session::has('error'))
                <p class="text-danger">{{session('error')}}</p>
            @endif
            <div class="tab-content mb-5">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">

                    <div class="card">
                        <div class="card-header">
                            <span class="align-middle">Job Overview</span>
                            @if(Auth::user()->role === 'applicant')
                                @if ($job_application !== null)
                                    @if ($detail->status === 1)
                                        <button
                                        @if ($job_application->application_status === "Received" or $job_application->application_status === "Reviewed" or $job_application->application_status === "Scheduled for Interview")
                                            class = "float-right btn-success btn"
                                        @else
                                            class = "float-right btn-secondary btn"
                                        @endif
                                        disabled >{{$job_application->application_status}}</button>
                                    @else 
                                        <button disabled class="float-right btn btn-secondary">Closed</button>
                                    @endif
                                @else
                                    @if ($detail->status === 1)
                                        <a href="{{url('jobs/apply/'.$detail->title.'/'.$detail->id)}}" class="float-right btn btn-dark">Apply Now</a>
                                    @else 
                                        <button disabled class="float-right btn btn-secondary">Closed</button>
                                    @endif
                                @endif
                            @else
                                @if ($detail->status === 1)
                                    <a  href="{{url('detail/'.$detail->title.'/'.$detail->id.'/'.'update-status/0')}}" class="float-right btn-danger btn">
                                        Close Job Application
                                    </a>
                                @else
                                    <a href="{{url('detail/'.$detail->title.'/'.$detail->id.'/'.'update-status/1')}}" class="float-right btn-warning btn">
                                        Open Job Application
                                    </a>
                                @endif
                            @endif
                        </div>
                        <div class="card-body">
                            <h2 class="font-weight-bold">{{$detail->title}}</h2>
                            <h4>by <a href="{{ url($detail->user->username) }}">{{ $detail->user->name }}</a></h4>
                            <br>
                            <h5 class="text-primary">Job Description</h5>
                            <hr>
                            <div class="p-2">{!! $detail->detail !!}</div>
                            <br>
                            <h5 class="text-primary">Additional Information</h5>
                            <hr>
                            {{-- <div class="card"> --}}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                        <h6 class="mb-0">Job Location</h6>
                                        </div>
                                        <div class="col-sm-9">
                                            {{$detail->workCity.", ".$detail->workProvince}}
                                        </div>
                                </div>
                                <hr>
                                <div class="row">
                                        <div class="col-sm-3">
                                        <h6 class="mb-0">Salary Range</h6>
                                        </div>
                                        <div class="col-sm-9">
                                            {{round($detail->salaryMin, 2)}} - {{round($detail->salaryMax, 2)}} PhP/month
                                        </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                    <h6 class="mb-0">Required Experience</h6>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$detail->requiredExperience}} years
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                    <h6 class="mb-0">Position</h6>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$detail->position}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                    <h6 class="mb-0">Contract</h6>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$detail->contract}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                    <h6 class="mb-0">Posted on</h6>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$detail->created_at}}
                                    </div>
                                </div>
                                </div>
                            {{-- </div> --}}
                        
                            
                            <div class="p-2"></div>
                        </div>
                        <div class="card-footer">
                            <h5>In <a href="{{url('jobs/search?searchCategory='.$detail->category->id)}}">{{$detail->category->title}}</a></h5>
                            <h6>Total Views : {{$detail->views}}</h6>
                        </div>
                    </div>
                </div>

        
                @include('post.partial.application_status', ['detail' => $detail])
                            
                @include('post.partial.applicants', ['detail' => $detail])
               


                <div class="tab-pane fade" id="comments" role="tabpanel">
                    @include('post.partial.comments', ['detail' => $detail])
                                
                </div>
            </div>
        </div>
    </div>

</div>



@endsection