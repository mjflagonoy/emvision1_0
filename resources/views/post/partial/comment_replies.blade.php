<!-- _comment_replies.blade.php -->


    @foreach($replies as $reply)
        <ul class="comments-list reply-list">
            <li>
                <!-- Avatar -->
                <div class="comment-avatar"><img src="https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png" alt=""></div>
                <!-- Contenedor del Comentario -->
                <div class="comment-box">
                    <div class="comment-head">
                        <h6 class="comment-name"><a href="{{ '/'.$reply->user->username}}">{{$reply->user->name}}</a></h6>
                        <span>{{ $reply->created_at }}</span>
                        {{-- <i class="fa fa-reply">reply</i> --}}
                    
                    </div>
                    <div class="comment-content">
                        {{$reply->reply}}
                    </div>
                </div>
            </li>
        </ul>
    @endforeach
