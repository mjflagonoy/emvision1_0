
@if (auth()->user()->role === "applicant" and $job_application !== null)
    @if($job_application->applicant_id === auth()->user()->id)
        <div class="tab-pane fade" id="applicationStatus" role="tabpanel">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Application Status</h5>
                </div>
                <div class="card-body">
                    <div class="panel-body">
                      @if($job_application->post_status === 1)

                        @if($job_application->application_status === "Received")
                          <div class="alert alert-info">
                            <strong class="default"><i class="fa fa-file-alt"></i></strong> Your application has been received.
                          
                          </div>
                        @elseif($job_application->application_status === "Reviewed")
                          <div class="alert alert-success">
                            <strong class="default"><i class="fa fa-file-alt"></i></strong> Your application is being reviewed.
                          
                          </div>
                        @elseif($job_application->application_status === "Scheduled for Interview")
                          <div class="alert alert-success">
                            <strong class="default"><i class="fa fa-file-alt"></i></strong> You have been scheduled for interview.
                          </div>

                          <div class="col-md-12 single-note-item all-category note-business" style="">
                            <div class="card card-body">
                                <span class="side-stick"></span>
                                <h5 class="note-title text-truncate w-75 mb-0" data-noteheading="Interview Details">Interview Details</h5>
                                <p class="note-date mb-0">{{ $job_application->interviewDate->format('F') .' '. $job_application->interviewDate->day . ", ".$job_application->interviewDate->year}}</p>
                                <h4 class="note-date text-muted mt-0">{{ $job_application->interviewTime->format('g:i A') }}</h4>
                                <br>
                                <h3>{{ $job_application->interviewPlatform }}</h3>
                                <div class="note-content">
                                    <div class="note-inner-content text-muted">{!! $job_application->interviewNote !!}</div>
                                </div>
                                
                            </div>
                        </div>
                        @elseif($job_application->application_status === "Not Suitable")
                          <div class="alert alert-danger">
                            <strong class="default"><i class="fa fa-file-alt"></i></strong> The employer has declined your application.
                          </div>
                        @elseif($job_application->application_status === "Void")
                          <div class="alert alert-danger">
                            <strong class="default"><i class="fa fa-file-alt"></i></strong> Your application has been voided.
                          </div>
                        @endif
                      @else 
                        <div class="alert alert-secondary nomargin">
                          <strong class="default"><i class="fa fa-file-alt"> This job post is</i> CLOSED </strong> and is no longer accepting applicants.
                         
                        </div>
                      @endif
                  </div>
                </div>
            </div>
        </div>

        <style>
          
        </style>
    @endif 
@endif

