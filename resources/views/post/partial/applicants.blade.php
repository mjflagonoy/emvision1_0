@if ($detail->user->id === auth()->user()->id)
<div class="tab-pane fade" id="applicants" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Applicants</h5>
        </div>
        <div class="card-body">
            @if ($job_application[0] ?? '')
                <div class="user-dashboard-info-box table-responsive mb-0 bg-white">
                    <form method="post" action="{{url('detail/'.Str::slug($detail->title).'/'.$detail->id.'/update-applicant-status')}}">
                        @csrf
                        <table class="table manage-candidates-top mb-0">
                            <thead>
                                <tr>
                                <th>Candidate Name</th>
                                <th class="text-center">Status</th>
                                <th class="action text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <span hidden>{{ $i = 0}}</span>
                                    @foreach($job_application as $job_app)
                                        
                                        <tr class="candidates-list">
                                            <td class="title">
                                                <div class="thumb">
                                                    <img class="img-fluid" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="">
                                                </div>
                                                <div class="candidate-list-details">
                                                    <div class="candidate-list-info">
                                                    <div class="candidate-list-title">
                                                        <h5 class="mb-0"><a href="{{'/'.$job_app->applicant->username}}">{{ $job_app->applicant->name }}</a></h5>
                                                    </div>
                                                    <div class="candidate-list-option">
                                                        <ul class="list-unstyled">
                                                        <li><i class="fas fa-map-marker-alt pr-1"></i>{{ $job_app->applicant->address }}</li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="candidate-list-favourite-time text-center">
                                                
                                                <span class="candidate-list-time order-1 mr-2 badge badge-dark"><small>{{ $job_app->application_status }}</small></span>
                                            </td>
                                            <td>
                                                <ul class="list-unstyled mb-0 d-flex justify-content-end">
                                                    <li  onclick="viewDetail({{$job_app}})"><a href="#" class="text-primary" data-toggle="tooltip" title="" data-original-title="view"><i class="fa fa-eye"></i></a></li>
                                                    <li class="text-primary" data-toggle="tooltip" title="" data-original-title="view">
                                                        <select id="{{"new_status".$job_app->id}}" onchange="statusListener({{$job_app}}, {{$i}})" name="applications[{{$i}}][job_status]" class="form-control btn-primary ml-2">
                                                            <option selected disabled>{{ $job_app->application_status }}</option>
                                                            <option value="Received">Received</option>
                                                            <option value="Reviewed">Reviewed</option>
                                                            <option value="Scheduled for Interview">Scheduled for Interview</option>
                                                            <option value="Not Suitable">Not Suitable</option>
                                                            <option value="Void">Void</option>
                                                        </select>
                                                    </li>
                                                </ul>
                                                <input name="applications[{{$i}}][job_id]" hidden value="{{ $job_app->id }}">
                                            </td>
                                        </tr>
                                        <span hidden>{{ $i++ }}</span>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="text-center mt-3 mt-sm-3">
                            <!-- Pagination -->
                            {{$job_application->links('vendor.pagination.custompagination')}}
                        </div>
                        <input name="submit" type="submit" class="btn btn-primary" value="Save Changes">
                    </form>
                    
                </div>  
            @else
                <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>  
            @endif  

                                  
        </div>
    </div>
</div>

<div class="modal fade" id="schedule-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
           <button class="float-left close-modal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           <h4 class="modal-title float-right">Schedule Interview</h4>
        </div>
        <form action="{{url('detail/'.Str::slug($detail->title).'/'.$detail->id.'/schedule-interview')}}" method="post">
            @csrf
            <input hidden name="job_id_modal" id="job_id_modal" type="number">
            <div class="modal-body">
                <div>
                    
                    <div class="mb-3"> 
                        <h5 class="font-weight-bold">What is the date of the interview?</h5>
                        <input data-format="dd/MM/yyyy" type="date" name="interviewDate" class="form-control">
                    </div>
                    <div class="mb-3"> 
                        <h5 class="font-weight-bold">What is the time of the interview?</h5>
                        <input data-format="hh:mm:ss" type="time" name="interviewTime" class="form-control">
                    </div>
                    <div class="mb-3"> 
                        <h5 class="font-weight-bold">Platform and Related Account(s)</h5>
                        <textarea rows="2" name="interviewPlatform"  class="form-control"></textarea>
                    </div>
                    <div class="mb-3"> 
                        <h5 class="font-weight-bold">Note to the applicant</h5>
                        <textarea class="ckeditor form-control" name="interviewNote" placeholder="Other things the applicant should take note of." rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Schedule Interview">
            </div>
        </form>
      </div>
    </div>
</div>


<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
           <button class="float-left close-modal" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           <h4 class="modal-title float-right">Application Detail</h4>
        </div>
        <div class="modal-body">
            <div class="candidate-list-details">
                <div class="candidate-list-info">
                <div class="candidate-list-title">
                    <h3 class="mb-0"><a href="" id="detail-alink"></a></h3>
                </div>
                <div class="candidate-list-option">
                    <ul class="list-unstyled">
                        <li id="detail-address"></li>
                        <li id="detail-email"></li>
                        
                    </ul>
                    <span class="badge badge-dark" id="detail-status"></span>
                    <br>
                
                    <h6 class="mb-0 text-secondary">Message From Applicant</h6>
                    <hr class="m-0">
                    <div id="detail-reason">
                    </div>
                    <a class="btn btn-primary mt-5" id="detail-resume">Download Resume</a>
                </div>
                </div>
            </div>
        </div>
        
      </div>
    </div>
</div>


<script>
    function statusListener(job_app){
        var e = document.getElementById("new_status"+job_app.id);
        var strStatus = e.options[e.selectedIndex].text;
        if(strStatus === "Scheduled for Interview"){
            $("#job_id_modal").val(job_app.id);
            $('#schedule-modal').modal('show');
            
        }
        
    }

    function viewDetail(job_app){
        document.getElementById("detail-alink").href="/"+job_app.applicant.username; 
        document.getElementById("detail-alink").text=job_app.applicant.name;
        document.getElementById("detail-address").innerHTML='<span class="fas fa-map-marker-alt pr-1"></span>'+job_app.applicant.address;
        document.getElementById("detail-email").innerHTML='<span class="fa fa-envelope pr-1"></span>'+job_app.applicant.email;
        document.getElementById("detail-reason").innerHTML=job_app.applicant_reason;
        document.getElementById("detail-resume").href="/download/"+job_app.id;
        $('#detail-modal').modal('show');
    }
</script>
<script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
        
        
<style>

.close-modal{
    background-color: transparent;
    background-repeat: no-repeat;
    border: none;
    cursor: pointer;
    overflow: hidden;
    outline: none;

}
/* user-dashboard-info-box */
.user-dashboard-info-box .candidates-list .thumb {
    margin-right: 20px;
}
.user-dashboard-info-box .candidates-list .thumb img {
    width: 80px;
    height: 80px;
    -o-object-fit: cover;
    object-fit: cover;
    overflow: hidden;
    border-radius: 50%;
}

.user-dashboard-info-box .title {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 30px 0;
}

.user-dashboard-info-box .candidates-list td {
    vertical-align: middle;
}

.user-dashboard-info-box td li {
    margin: 0 4px;
}

.user-dashboard-info-box .table thead th {
    border-bottom: none;
}

.table.manage-candidates-top th {
    border: 0;
}

.user-dashboard-info-box .candidate-list-favourite-time .candidate-list-favourite {
    margin-bottom: 10px;
}

.table.manage-candidates-top {
    min-width: 650px;
}

.user-dashboard-info-box .candidate-list-details ul {
    color: #969696;
}

/* Candidate List */
.candidate-list {
    background: #ffffff;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    border-bottom: 1px solid #eeeeee;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 20px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
.candidate-list:hover {
    -webkit-box-shadow: 0px 0px 34px 4px rgba(33, 37, 41, 0.06);
    box-shadow: 0px 0px 34px 4px rgba(33, 37, 41, 0.06);
    position: relative;
    z-index: 99;
}
.candidate-list:hover a.candidate-list-favourite {
    color: #e74c3c;
    -webkit-box-shadow: -1px 4px 10px 1px rgba(24, 111, 201, 0.1);
    box-shadow: -1px 4px 10px 1px rgba(24, 111, 201, 0.1);
}

.candidate-list .candidate-list-image {
    margin-right: 25px;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 80px;
    flex: 0 0 80px;
    border: none;
}
.candidate-list .candidate-list-image img {
    width: 80px;
    height: 80px;
    -o-object-fit: cover;
    object-fit: cover;
}

.candidate-list-title {
    margin-bottom: 5px;
}

.candidate-list-details ul {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-bottom: 0px;
}
.candidate-list-details ul li {
    margin: 5px 10px 5px 0px;
    font-size: 13px;
}

.candidate-list .candidate-list-favourite-time {
    margin-left: auto;
    text-align: center;
    font-size: 13px;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 90px;
    flex: 0 0 90px;
}
.candidate-list .candidate-list-favourite-time span {
    display: block;
    margin: 0 auto;
}
.candidate-list .candidate-list-favourite-time .candidate-list-favourite {
    display: inline-block;
    position: relative;
    height: 40px;
    width: 40px;
    line-height: 40px;
    border: 1px solid #eeeeee;
    border-radius: 100%;
    text-align: center;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    margin-bottom: 20px;
    font-size: 16px;
    color: #646f79;
}
.candidate-list .candidate-list-favourite-time .candidate-list-favourite:hover {
    background: #ffffff;
    color: #e74c3c;
}

.candidate-banner .candidate-list:hover {
    position: inherit;
    -webkit-box-shadow: inherit;
    box-shadow: inherit;
    z-index: inherit;
}

.bg-white {
    background-color: #ffffff !important;
}
</style>
@endif

