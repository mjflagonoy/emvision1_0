<div class="card">
    <div class="card-header">
        <h5 class="card-title mb-0">Comments</h5>
    </div>
    <div class="card-body">
        
        @auth
        <!-- Add Comment -->
       
            <div class="card-body">
                <h5 >Add Comment</h5>
                <form method="post" action="{{url('save-comment/'.Str::slug($detail->title).'/'.$detail->id)}}">
                    @csrf
                    <textarea name="comment" class="form-control"></textarea>
                    <div class="form-check form-switch mt-3">
                        <input class="form-check-input" type="checkbox" name="isPublic" checked>
                        <h5>Display publicly?</h5>
                    </div>
                    <input type="submit" class="btn btn-dark mt-2" value="Comment"/>
                </form>
            </div>
        
        @endauth
        
                <h5 class="ml-3 mt-3">Previous Comments</h5>
                <hr>
                @if($detail->comments)
                    @foreach($detail->comments as $comment)
                        @php
                            $display = false;
                            $display = $comment->isPublic();
                            if (auth()->user()->id === $comment->user_id) {
                                $display = true;
                            }
                            
                            if(auth()->user()->id === $detail->userId){
                                $display = true;
                            }

                            if(auth()->user()->role === 'admin'){
                                $display = true;
                            }
                        @endphp

                        @if ($display)
                            <ul id="comments-list" class="comments-list">
                                <li>
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar"><img src="https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png" alt=""></div>
                                        <!-- Comment content -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                
                                                <h6 class="comment-name"><a href="{{'/'.$comment->user->username}}"> {{$comment->user->name}}</a></h6>
                                                @if($comment->isPublic) 
                                                    <span class="fa fa-globe mr-1" aria-hidden="true"></span>
                                                @else 
                                                    <span class="fa fa-eye-slash mr-1" aria-hidden="true"></span>
                                                @endif
                                                <span>{{ $comment->created_at }}</span>
                                                <i class="fa fa-reply" id="{{'reply'.$comment->id }}" onclick="addReplyForm({{ $comment->id }})">reply</i>
                                            </div>
                                            <div class="comment-content">
                                                {{$comment->comment}}
                                            </div>
                                            <div id="{{ 'reply-div'.$comment->id }}">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    @include('post.partial.comment_replies', ['replies' => $comment->replies, 'post_id' => $detail->id])
                                    
                                </li>
                            </ul>
                        @endif
                    @endforeach
                @endif
    </div>
</div>


<script type="text/javascript">
    

    function addReplyForm(replyId){
        var postId = '{{ $detail->id }}', userId = '{{ $detail->userId }}';
        document.getElementById("reply"+replyId).style.visibility = "hidden";
        var str = "#reply-div"+replyId;
        $(str).append('<form class="m-2" method="post" action="/save-reply/'+ postId + '/' + replyId +'">@csrf<input name="reply" class="form-control col-sm-10 float-left" type="text"><input class="btn btn-dark btn-xs col-sm-2 p-0 float-right" type="submit" value="Reply"></form>');
    }

</script>

<style>
    /**
     * Oscuro: #283035
     * Azul: #03658c
     * Detalle: #c7cacb
     * Fondo: #dee1e3
     ----------------------------------*/
    /* *{
         margin: 0;
         padding: 0;
         -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
         box-sizing: border-box;
     }
     */
     a {
         color: #03658c;
         text-decoration: none;
     }
    
    /* ul {
        list-style-type: none;
    } */
    
    /* body {
        font-family: 'Roboto', Arial, Helvetica, Sans-serif, Verdana;
        background: #dee1e3;
    }
     */
    
    .comments-container {
        margin: 60px auto 15px;
        width: 768px;
    }
    
    .comments-container h1 {
        font-size: 36px;
        color: #283035;
        font-weight: 400;
    }
    
    .comments-container h1 a {
        font-size: 18px;
        font-weight: 700;
    }
    
    .comments-list {
        margin-top: 30px;
        position: relative;
    }
    
    .comments-list:before {
        content: '';
        width: 2px;
        height: 100%;
        background: #c7cacb;
        position: absolute;
        left: 32px;
        top: 0;
    }
    
    .comments-list:after {
        content: '';
        position: absolute;
        background: #c7cacb;
        bottom: 0;
        left: 27px;
        width: 7px;
        height: 7px;
        border: 3px solid #dee1e3;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
    }
    
    .reply-list:before, .reply-list:after {display: none;}
    .reply-list li:before {
        content: '';
        width: 60px;
        height: 2px;
        background: #c7cacb;
        position: absolute;
        top: 25px;
        left: -55px;
    }
    
    
    .comments-list li {
        margin-bottom: 15px;
        display: block;
        position: relative;
    }
    
    .comments-list li:after {
        content: '';
        display: block;
        clear: both;
        height: 0;
        width: 0;
    }
    
    .reply-list {
        padding-left: 50px;
        clear: both;
        margin-top: 15px;
    }
    /**
     * Avatar
     ---------------------------*/
    .comments-list .comment-avatar {
        width: 40px;
        height: 40px;
        position: relative;
        z-index: 99;
        float: left;
        border: 3px solid #FFF;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        overflow: hidden;
    }
    
    .comments-list .comment-avatar img {
        width: 100%;
        height: 100%;
    }
    
    .reply-list .comment-avatar {
        width: 35px;
        height: 35px;
    }
    
    .comment-main-level:after {
        content: '';
        width: 0;
        height: 0;
        display: block;
        clear: both;
    }
    
    .comments-list .comment-box {
        width: 92%;
        float: right;
        position: relative;
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
        -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
        box-shadow: 0 1px 1px rgba(0,0,0,0.15);
    }
    
    .comments-list .comment-box:before, .comments-list .comment-box:after {
        content: '';
        height: 0;
        width: 0;
        position: absolute;
        display: block;
        border-width: 10px 12px 10px 0;
        border-style: solid;
        border-color: transparent #FCFCFC;
        top: 8px;
        left: -11px;
    }
    
    .comments-list .comment-box:before {
        border-width: 11px 13px 11px 0;
        border-color: transparent rgba(0,0,0,0.05);
        left: -12px;
    }
    
    .reply-list .comment-box {
        width: 92%;
    }
    .comment-box .comment-head {
        background: #FCFCFC;
        padding: 10px 12px;
        border-bottom: 1px solid #E5E5E5;
        overflow: hidden;
        -webkit-border-radius: 4px 4px 0 0;
        -moz-border-radius: 4px 4px 0 0;
        border-radius: 4px 4px 0 0;
    }
    
    .comment-box .comment-head i {
        float: right;
        margin-left: 0px;
        position: relative;
        top: 2px;
        color: #A6A6A6;
        cursor: pointer;
        -webkit-transition: color 0.3s ease;
        -o-transition: color 0.3s ease;
        transition: color 0.3s ease;
    }
    
    .comment-box .comment-head i:hover {
        color: #03658c;
    }
    
    .comment-box .comment-name {
        color: #283035;
        font-size: 14px;
        font-weight: 700;
        float: left;
        margin-right: 10px;
    }
    
    .comment-box .comment-name a {
        color: #283035;
    }
    
    .comment-box .comment-head span {
        float: left;
        color: #999;
        font-size: 13px;
        position: relative;
        top: 1px;
    }
    
    .comment-box .comment-content {
        background: #FFF;
        padding: 12px;
        font-size: 15px;
        color: #595959;
        -webkit-border-radius: 0 0 4px 4px;
        -moz-border-radius: 0 0 4px 4px;
        border-radius: 0 0 4px 4px;
    }
    
    .comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {color: #03658c;}
    .comment-box .comment-name.by-author:after {
        content: 'autor';
        background: #03658c;
        color: #FFF;
        font-size: 12px;
        padding: 3px 5px;
        font-weight: 700;
        margin-left: 10px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }
    
    /** =====================
     * Responsive
     ========================*/
    @media only screen and (max-width: 766px) {
        .comments-container {
            width: 480px;
        }
    
        .comments-list .comment-box {
            width: 390px;
        }
    
        .reply-list .comment-box {
            width: 320px;
        }
    }
    </style>
