@extends('layouts.app')

@section('content')


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<div class="container p-0">
    <!-- Breadcrumb -->
    <nav aria-label="breadcrumb" class="main-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url(''. auth()->user()->username )}}">Profile</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
      </nav>
      <!-- /Breadcrumb -->
      
      <div class="row">
        <div class="col-md-5 col-xl-4">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Profile Settings</h5>
                </div>

                <div class="list-group list-group-flush" role="tablist">
                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#profilecompany" role="tab">
                      About Company
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-7 col-xl-8">
            <div class="tab-content mb-5">
                <div class="tab-pane fade show active" id="profilecompany" role="tabpanel">

                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">About Company</h5>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('profile/save-company-information') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="inputCompanyname">Name</label>
                                            <textarea name="inputCompanyname" rows="2" class="form-control" id="inputCompanyname" placeholder="Company Name">{{ $currentUser['user']->name}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail">Email</label>
                                            <textarea name="inputEmail" rows="2" class="form-control" id="inputEmail" placeholder="Company Email">{{ $currentUser['user']->email}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputWebsite">Website</label>
                                            <textarea name="inputWebsite" rows="2" class="form-control" id="inputWebsite" placeholder="Website">{{ $currentUser['user']->website}}</textarea>
                                        </div>
                                        <div class="form-group">
                                        <label for="inputCompanySize">Company Size</label>
                                                <input name="inputCompanySize" type="number" id="inputCompanySize" class="form-control" value = "{{ $currentUser['user']->company_size}}">
                                                  
                                            </textarea>  
                                        </div>
                                        <div class="form-group">
                                            <label for="inputIndustry">Industry</label>
                                            <textarea name="inputIndustry" rows="2" class="form-control" id="inputIndustry" placeholder="Industry">{{ $currentUser['user']->company_industry}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-center">
                                            <img alt="Andrew Jones" src="https://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                            <div class="mt-2">
                                                <span class="btn btn-primary"><i class="fa fa-upload"></i>Upload</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputCompanyType">Type</label>
                                        <input name="inputCompanyType" type="text" class="form-control" id="inputCompanyType" placeholder="type" value="{{ $currentUser['user']->company_type }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputCompanySpecialties">Specialties</label>
                                        <input name="inputCompanySpecialties" type="text" class="form-control" id="inputCompanySpecialties" placeholder="Speacilities" value="{{ $currentUser['user']->company_specialties }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputLocations">Locations</label>
                                        <input name="inputLocations" type="text" class="form-control" id="inputLocations" placeholder="Locations" value="{{ $currentUser['user']->company_locations }}">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </form>

                        </div>
                    </div>
                </div>

                   
            </div>
        </div>
    </div>

</div>


@endsection

