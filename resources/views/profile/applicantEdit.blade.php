@extends('layouts.app')

@section('content')


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<div class="container p-0">
    <!-- Breadcrumb -->
    <nav aria-label="breadcrumb" class="main-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url(''. auth()->user()->username )}}">Profile</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
      </nav>
      <!-- /Breadcrumb -->

    <div class="row">
        <div class="col-md-5 col-xl-4">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Profile Settings</h5>
                </div>

                <div class="list-group list-group-flush" role="tablist">
                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account" role="tab">
                      Account
                    </a>
                    <a class="list-group-item list-group-item-action" hidden data-toggle="list" href="#password" role="tab">
                      Password
                    </a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#vitae" role="tab">
                      Curriculum Vitae
                    </a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#delete" role="tab">
                      Delete account
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-7 col-xl-8">
            <div class="tab-content mb-5">
                <div class="tab-pane fade show active" id="account" role="tabpanel">

                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Public info</h5>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('profile/save-information') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="inputFullname">Fullname</label>
                                            <input onclick='listen("inputFullname")' name="inputFullname" type="text" class="form-control" id="inputFullname" placeholder="Fullname" value="{{ $currentUser['user']->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputBio">Biography</label>
                                            <textarea onclick='listen("inputBio")' name="inputBio" rows="2" class="form-control" id="inputBio" placeholder="Tell something about yourself">{{ $currentUser['user']->biography }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-center">
                                            <img alt="Andrew Jones" src="https://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                            <div class="mt-2">
                                                <span class="btn btn-primary"><i class="fa fa-upload"></i>Upload</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="inputAddress">Current Address</label>
                                            <input onclick='listen("inputAddress")' name="inputAddress" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" value="{{ $currentUser['user']->address }}">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="inputEmail">Email</label>
                                        <input onclick='listen("inputEmail")' name="inputEmail" type="Email" class="form-control" id="inputEmail" placeholder="example@emvision.com" value="{{ $currentUser['user']->email }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputNumber">Contact Number</label>
                                        <input onclick='listen("inputNumber")' name="inputNumber" type="number" class="form-control" id="inputNumber" placeholder="Contact Number" value="{{ $currentUser['user']->number }}">
                                    </div>
                                </div>

                               

                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="password" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Manage Password</h5>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label for="inputPasswordCurrent">Current password</label>
                                    <input type="password" class="form-control" id="inputPasswordCurrent">
                                    <small><a href="#">Forgot your password?</a></small>
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordNew">New password</label>
                                    <input type="password" class="form-control" id="inputPasswordNew">
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordNew2">Verify password</label>
                                    <input type="password" class="form-control" id="inputPasswordNew2">
                                </div>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="vitae" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Manage Curriculum Vitae</h5>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="education-tab" data-toggle="tab" href="#education" role="tab" aria-controls="education" aria-selected="false">Edit</a>
                                </li>
    
                              </ul>

                              <div class="tab-content" id="myTabContent">

                                <div class="tab-pane fade show active" id="education" role="tabpanel" aria-labelledby="education-tab">
                                    
                                    <form method="POST" action="{{ route('profile/save-education') }}">
                                        @csrf
                                        <div id="dynamicAddRemove">
                                            
                                            @if (isset($currentUser['educations'][0]) )
                                                @for ($i = 0; $i < $currentUser['educations']->count(); $i++)
                                                    <div class="card mt-2 mb-2 edu" >
                                                       
                                                        <div class="card-body">
                                                            <input name="education[{{$i}}][id]" hidden value="{{ $currentUser['educations'][$i]->id }}">
                                                                <button type="button" @if($i!=0) onclick="deleteEdu({{ $currentUser['educations'][$i]->id }})" @endif class="close @if($i!=0) text-danger remove-tr @else text-primary @endif" data-dismiss="alert" aria-label="Add Education"  name="add" id="add-btn">
                                                                    <span aria-hidden="true">@if($i!=0) &times; Delete @else &plus; Add @endif</span>
                                                                </button>
                                                           
                                                            <div class="form-row">
                                                                <div class="form-group col-md-8">
                                                                    <label for="inputSchool">Institute/University/School</label>
                                                                    <input onclick='listen("inputSchool")' name="education[{{$i}}][school]" type="text" class="form-control" id="inputSchool" placeholder="Enter the name of the school" value="{{ $currentUser['educations'][$i]->school }}">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="inputQualification">Level</label>
                                                                    <select name="education[{{$i}}][level]" id="inputQualification" class="form-control">
                                                                        <option @if($currentUser['educations'][$i]->level == "Primary") selected="selected" @endif>Primary</option>
                                                                        <option @if($currentUser['educations'][$i]->level == "Secondary") selected="selected" @endif>Secondary</option>
                                                                        <option @if($currentUser['educations'][$i]->level == "Upper-secondary") selected="selected" @endif>Upper-secondary</option>
                                                                        <option @if($currentUser['educations'][$i]->level == "Vocational") selected="selected" @endif>Vocational</option>
                                                                        <option @if($currentUser['educations'][$i]->level == "Tertiary") selected="selected" @endif>Tertiary</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-8">
                                                                    <label for="inputField">Field of Study</label>
                                                                    <input onclick='listen("field")' name="education[{{$i}}][field]" type="text" class="form-control" id="inputField" placeholder="Enter the field of study" value="{{ $currentUser['educations'][$i]->field }}">
                                                                </div>
                                                                
                                                                <div class="form-group col-md-4">
                                                                    <label for="inputYear">Graduation</label>
                                                                    <input onclick='listen("year")' name="education[{{$i}}][year]" type="number" class="form-control" id="inputYear" placeholder="Enter the year of graduation" value="{{ $currentUser['educations'][$i]->yearGraduation }}">
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                @endfor
                                            @else
                                                <div class="card mt-2 mb-2 edu" >
                                                    <div class="card-body">
                                                        <input name="education[0][id]" hidden value="{{ null }}">
                                                        <button type="button" class="close text-primary" data-dismiss="alert" aria-label="Add Education"  name="add" id="add-btn">
                                                            <span aria-hidden="true">&plus;</span>
                                                        </button>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputSchool">Institute/University/School</label>
                                                                <input onclick='listen("inputSchool")' name="education[0][school]" type="text" class="form-control" id="inputSchool" placeholder="Enter the name of the school">
                                                                @error('education[0][school]')
                                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputQualification">Level</label>
                                                                <select name="education[0][level]" id="inputQualification" class="form-control">
                                                                    <option selected="">Primary</option>
                                                                    <option>Secondary</option>
                                                                    <option>Upper-secondary</option>
                                                                    <option>Vocational</option>
                                                                    <option>Tertiary</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputField">Field of Study</label>
                                                                <input onclick='listen("inputField")' name="education[0][field]" type="text" class="form-control" id="inputField" placeholder="Enter the field of study">
                                                            </div>
                                                            
                                                            <div class="form-group col-md-4">
                                                                <label for="inputYear">Graduation</label>
                                                                <input onclick='listen("inputYear")' name="education[0][year]" type="number" class="form-control" id="inputYear" placeholder="Enter the year of graduation">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        <div id="experienceCard">
                                            @if (isset($currentUser['experiences'][0]))
                                                @for ($i = 0; $i < $currentUser['experiences']->count(); $i++)
                                                    <div class="card mt-2 mb-2 edu" >
                                                        <div class="card-body">
                                                            <input name="work[{{$i}}][id]" hidden value="{{ $currentUser['experiences'][$i]->id }}">
                                                            <button type="button" @if($i!=0) onclick="deleteExperience({{ $currentUser['experiences'][$i]->id }})" @endif class="close @if($i!=0) text-danger remove-tr @else text-primary @endif" data-dismiss="alert" aria-label="Add Experiences"  name="addExperience" id="add-btn-experience">
                                                                <span aria-hidden="true">@if($i!=0) &times; Delete @else &plus; Add @endif</span>
                                                            </button>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-12">
                                                                    <label for="inputCompany">Company</label>
                                                                    <input onclick='listen("inputCompany")' name="work[{{$i}}][company]" value="{{ $currentUser['experiences'][$i]->company }}" type="text" class="form-control" id="inputCompany" placeholder="Enter company name">
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="inputPosition">Position</label>
                                                                    <input onclick='listen("inputPosition")' name="work[{{$i}}][position]" value="{{ $currentUser['experiences'][$i]->position }}" type="text" class="form-control" id="inputPosition" placeholder="Enter position">
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label for="inputLocation">Company Location</label>
                                                                    <input onclick='listen("inputLocation")' name="work[{{$i}}][location]" value="{{ $currentUser['experiences'][$i]->location }}" type="text" class="form-control" id="inputLocation" placeholder="Enter the location of your company">
                                                                </div> 
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="inputYearEmployment">Year of Employment</label>
                                                                    <input onclick='listen("inputYearEmployment")' name="work[{{$i}}][yearEmployment]" value="{{ $currentUser['experiences'][$i]->yearEmployment }}" type="number" class="form-control" id="inputYearEmployment" placeholder="Enter starting year">
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label for="inputYearEnd">Last Year Attended</label>
                                                                    <input onclick='listen("inputYearEnd")' name="work[{{$i}}][yearEnd]" value="{{ $currentUser['experiences'][$i]->yearEnd }}" type="number" class="form-control" id="inputYearEnd" placeholder="Enter latest year of attendance">
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endfor
                                            @else
                                                <div class="card mt-2 mb-2 edu" >
                                                    <div class="card-body">
                                                        <input name="work[0][id]" hidden value="{{ null }}">
                                                        <button type="button" class="close text-primary" data-dismiss="alert" aria-label="Add Experience"  name="addExperience" id="add-btn-experience">
                                                            <span aria-hidden="true">&plus;</span>Add
                                                        </button>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputCompany">Company</label>
                                                                <input onclick='listen("inputCompany")' name="work[0][company]" type="text" class="form-control" id="inputCompany" placeholder="Enter company name">
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPosition">Position</label>
                                                                <input onclick='listen("inputPosition")' name="work[0][position]" type="text" class="form-control" id="inputPosition" placeholder="Enter position">
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label for="inputLocation">Company Location</label>
                                                                <input onclick='listen("inputLocation")' name="work[0][location]" type="text" class="form-control" id="inputLocation" placeholder="Enter the location of your company">
                                                            </div> 
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputYearEmployment">Year of Employment</label>
                                                                <input onclick='listen("inputYearEmployment")' name="work[0][yearEmployment]" type="number" class="form-control" id="inputYearEmployment" placeholder="Enter starting year">
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label for="inputYearEnd">Last Year Attended</label>
                                                                <input onclick='listen("inputYearEnd")' name="work[0][yearEnd]" type="number" class="form-control" id="inputYearEnd" placeholder="Enter latest year of attendance">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        <div id="skill-card">

                                            @if(isset($currentUser['skills'][0]))
                                                @for ($i = 0; $i < $currentUser['skills']->count(); $i++)
                                                    <div class="card mt-2 mb-2 skill" >
                                                        <div class="card-body">
                                                            <input name="skill[{{ $i }}][id]" hidden value="{{ $currentUser['skills'][$i]->id }}">
                                                            
                                                            <button type="button" @if($i!=0) onclick="deleteSkill({{ $currentUser['skills'][$i]->id }})" @endif class="close @if($i!=0) text-danger @else text-primary @endif" data-dismiss="alert" aria-label="Add Skill"  name="add-skill" id="add-skill-btn">
                                                                <span aria-hidden="true">@if($i!=0) &times; Delete @else &plus; Add @endif</span>
                                                            </button>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-8">
                                                                    <label for="inputField">Skill</label>
                                                                    <input onclick='listen("inputField")' name="skill[{{ $i }}][skill]" value="{{ $currentUser['skills'][$i]->skill }}" type="text" class="form-control" id="inputField" placeholder="Enter the field of study">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="customRange1" class="form-label">Proficiency Level</label><br>
                                                                    <input name="skill[{{ $i }}][proficiency]" value="{{ $currentUser['skills'][$i]->proficiency }}" type="range" class="form-range" min="0" max="100" id="customRange1">        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endfor
                                            @else
                                                <div class="card mt-2 mb-2 skill" >
                                                    <div class="card-body">
                                                        <input name="skill[0][id]" hidden value="{{ null }}">
                                                        <button type="button" class="close text-primary" data-dismiss="alert" aria-label="Add Skill"  name="add-skill" id="add-skill-btn">
                                                            <span aria-hidden="true">&plus;</span>
                                                        </button>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputField">Skill</label>
                                                                <input onclick='listen("inputField")' name="skill[0][skill]" type="text" class="form-control" id="inputField" placeholder="Enter the field of study">
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="customRange1" class="form-label">Proficiency Level</label><br>
                                                                <input name="skill[0][proficiency]" type="range" class="form-range" min="0" max="100" id="customRange1">        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                        </div>

                                        <div id="certification-card">

                                            @if(isset($currentUser['awards'][0]))
                                                @for ($i = 0; $i < $currentUser['awards']->count(); $i++)
                                                    <div class="card mt-2 mb-2 edu" >
                                                        <div class="card-body">
                                                            <input name="certificate[{{$i}}][id]" hidden value="{{ $currentUser['awards'][$i]->id }}">
                                                           
                                                            <button type="button" @if($i!=0) onclick="deleteAward({{ $currentUser['awards'][$i]->id }})" @endif class="close @if($i!=0) text-danger @else text-primary @endif" data-dismiss="alert"  aria-label="Add Award"  name="add-award" id="add-award-btn">
                                                                <span aria-hidden="true">@if($i!=0) &times; Delete @else &plus; Add @endif</span>
                                                            </button>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-12">
                                                                    <label for="inputAward">Certification/Award/Achievement</label>
                                                                    <input onclick='listen("inputAward")' name="certificate[{{$i}}][award]" value="{{ $currentUser['awards'][$i]->award }}" type="text" class="form-control" id="inputAward" placeholder="Enter the name of your achievement">
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-8">
                                                                    <label for="inputCertifyingBody">Certifying Organization</label>
                                                                    <input onclick='listen("inputCertifyingBody")' name="certificate[{{$i}}][certifyingBody]" value="{{ $currentUser['awards'][$i]->certifyingBody }}" type="text" class="form-control" id="inputcertifyingBody" placeholder="Enter the certifying organization or body">
                                                                </div>
                                                                
                                                                <div class="form-group col-md-4">
                                                                    <label for="inputYearObtainment">Year of Obtainment</label>
                                                                    <input onclick='listen("inputYearObtainment")' name="certificate[{{$i}}][yearOfObtainment]" value="{{ $currentUser['awards'][$i]->awardYear }}" type="number" class="form-control" id="yearObtainment" placeholder="Enter the year of obtainment of the award">
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                @endfor
                                            @else 
                                                <div class="card mt-2 mb-2 edu" >
                                                    <div class="card-body">
                                                        <input name="certificate[0][id]" hidden value="{{ null }}">
                                                        <button type="button" class="close text-primary" data-dismiss="alert" aria-label="Add Award"  name="add-award" id="add-award-btn">
                                                            <span aria-hidden="true">&plus;</span>
                                                        </button>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="inputAward">Certification/Award/Achievement</label>
                                                                <input onclick='listen("inputAward")' name="certificate[0][award]" type="text" class="form-control" id="inputAward" placeholder="Enter the name of your achievement">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputCertifyingBody">Certifying Organization</label>
                                                                <input onclick='listen("inputCertifyingBody")' name="certificate[0][certifyingBody]" type="text" class="form-control" id="inputcertifyingBody" placeholder="Enter the certifying organization or body">
                                                            </div>
                                                            
                                                            <div class="form-group col-md-4">
                                                                <label for="inputYearObtainment">Year of Obtainment</label>
                                                                <input onclick='listen("inputYearObtainment")' name="certificate[0][yearOfObtainment]" type="number" class="form-control" id="yearObtainment" placeholder="Enter the year of obtainment of the award">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            @endif

                                            
                                        </div>

                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                        
                                    </form>
                                    
                                </div>

                            

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="delete" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Delete Account</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('profile/delete-applicant') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="inputPasswordNew2">Your account will be deactivated. Please click the <i>deactivate</i> button to proceed.</label>
                                    {{-- <input type="password" class="form-control" id="inputPasswordNew2" placeholder="Enter password"> --}}
                                </div>
                                <button type="submit" class="btn btn-danger">Deactivate</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var i = '{{ $currentUser['educations']->count() }}', experience = '{{ $currentUser['experiences']->count() }}', award = '{{ $currentUser['awards']->count() }}', skill = '{{ $currentUser['skills']->count() }}';
    if(i > 0) --i;
    $("#add-btn").click(function(){
        ++i;
        $("#dynamicAddRemove").append('<div class="card mt-2 mb-2 edu" ><input name="education['+i+'][id]" hidden value="{{ null }}"><div class="card-body"><button type="button" class="close text-danger remove-tr" data-dismiss="alert" aria-label="Add Education"><span aria-hidden="true">&times; Delete</span></button><div class="form-row"><div class="form-group col-md-8"><label for="inputSchool">Institute/University/School</label><input name="education['+i+'][school]" type="text" class="form-control" id="inputSchool" placeholder="Enter the name of the school"> </div><div class="form-group col-md-4"> <label for="inputQualification">Level</label> <select name="education['+i+'][level]" id="inputQualification" class="form-control"> <option selected="">Primary</option><option>Secondary</option> <option>Upper-secondary</option><option>Vocational</option><option>Tertiary</option></select></div></div><div class="form-row"><div class="form-group col-md-8"><label for="inputField">Field of Study</label><input name="education['+i+'][field]" type="text" class="form-control" id="inputField" placeholder="Enter the field of study"></div><div class="form-group col-md-4"><label for="inputYear">Graduation</label><input name="education['+i+'][year]" type="text" class="form-control"id="inputYear"placeholder="Enter the year of graduation"></div></div></div></div>');
    });
    function deleteEdu(url){
        if(url === null) $(this).parents('div.edu').remove();
        else window.location.href = "/profile/delete-education/" + url;
    }
    $(document).on('click', '.remove-tr', function(){  
        $(this).parents('div.edu').remove();
    });

    if(experience > 0) --experience;
    $("#add-btn-experience").click(function(){
        ++experience;
        $("#experienceCard").append('<div class="card mt-2 mb-2 work" ><div class="card-body"> <input name="work['+experience+'][id]" hidden value="{{ null }}"> <button type="button" class="close text-danger remove-experience" data-dismiss="alert" aria-label="Remove Experience"  name="removeExperience" id="remove-experience"><span aria-hidden="true">&times; Delete</span></button><div class="form-row"><div class="form-group col-md-12"><label for="inputCompany">Company</label> <input name="work['+experience+'][company]" type="text" class="form-control" id="inputCompany" placeholder="Enter company name"></div></div><div class="form-row"><div class="form-group col-md-6"><label for="inputPosition">Position</label> <input name="work['+experience+'][position]" type="text" class="form-control" id="inputPosition" placeholder="Enter position"> </div><div class="form-group col-md-6"><label for="inputLocation">Company Location</label><input name="work['+experience+'][location]" type="text" class="form-control" id="inputLocation" placeholder="Enter the location of your company"></div></div><div class="form-row"><div class="form-group col-md-6"><label for="inputYearEmployment">Year of Employment</label><input name="work['+experience+'][yearEmployment]" type="number" class="form-control" id="inputYearEmployment" placeholder="Enter starting year"></div><div class="form-group col-md-6"><label for="inputYearEnd">Last Year Attended</label><input name="work['+experience+'][yearEnd]" type="number" class="form-control" id="inputYearEnd" placeholder="Enter latest year of attendance"></div> </div></div></div>');
    });
    function deleteExperience(url){
        if(url === null) $(this).parents('div.work').remove();
        else window.location.href = "/profile/delete-experience/" + url;
    }
    $(document).on('click', '.remove-experience', function(){  
        $(this).parents('div.work').remove();
    });

    if(award > 0) --award;
    $("#add-award-btn").click(function(){
        ++award;
        $("#certification-card").append('<div id="certification-card"><div class="card mt-2 mb-2 certificate" ><div class="card-body"> <input name="certificate['+award+'][id]" hidden value="{{ null }}"> <button type="button" class="close text-danger remove-award" data-dismiss="alert" aria-label="Remove Award"  name="remove-award" id="remove-award-btn"><span aria-hidden="true">&times; Delete</span></button><div class="form-row"><div class="form-group col-md-12"><label for="inputAward">Certification/Award/Achievement</label><input name="certificate['+award+'][award]" type="text" class="form-control" id="inputAward" placeholder="Enter the name of your achievement"></div></div><div class="form-row"><div class="form-group col-md-8"><label for="inputCertifyingBody">Certifying Organization</label><input name="certificate['+award+'][certifyingBody]" type="text" class="form-control" id="inputcertifyingBody" placeholder="Enter the certifying organization or body"></div><div class="form-group col-md-4"><label for="inputYearOfObtainment">Year of Obtainment</label><input name="certificate['+award+'][yearOfObtainment]" type="number" class="form-control" id="yearOfObtainment" placeholder="Enter the year of obtainment of the award"></div></div></div></div></div>');
    });
    function deleteAward(url){
        if(url === null) $(this).parents('div.certificate').remove();
        else window.location.href = "/profile/delete-award/" + url;
    }
    $(document).on('click', '.remove-award', function(){  
        $(this).parents('div.certificate').remove();
    });
    if(skill > 0) --skill;
    $("#add-skill-btn").click(function(){
        ++skill;
        $("#skill-card").append('<div id="skill-card"><div class="card mt-2 mb-2 skill" ><div class="card-body"> <input name="skill['+skill+'][id]" hidden value="{{ null }}"> <button type="button" class="close text-danger remove-skill" data-dismiss="alert" aria-label="Add Skill"  name="add-skill" id="add-skill-btn"><span aria-hidden="true">&times; Delete</span></button><div class="form-row"><div class="form-group col-md-8"><label for="inputField">Skill</label><input name="skill['+skill+'][skill]" type="text" class="form-control" id="inputField" placeholder="Enter the field of study"></div><div class="form-group col-md-4"><label for="customRange1" class="form-label">Proficiency Level</label><br><input name="skill['+skill+'][proficiency]" type="range" class="form-range" min="0" max="100" id="customRange1"></div></div></div></div></div>');
    });
    function deleteSkill(url){
        if(url === null) $(this).parents('div.skill').remove();
        else window.location.href = "/profile/delete-skill/" + url;
    }
    $(document).on('click', '.remove-skill', function(){  
        $(this).parents('div.skill').remove();
    });
 </script>

@endsection
