@extends('layouts.app')

@section('content')

<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1573035929/Pra/59d280613165c000014cc031_circleniamhhogan.png" alt="Admin" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4>{{ $userProfile['user']->name }}</h4>
                      <p class="text-muted font-size-sm">
                        @if (isset($userProfile['user']->biography))
                            {{ $userProfile['user']->biography }}
                        @else
                            <h5 class="text-secondary">None</h5>
                        @endif  
                      </p>

                      @auth
                        @if ($userProfile['user']->username == auth()->user()->username)
                          <a class="btn btn-primary" href="{{ route('profile/edit-company') }}">Edit Profile</a>    
                        @endif 
                      @endauth
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <div class="card-header">
                  <h5 class="card-title mb-0">Company Rating</h5>
              </div>
                <div class="card-body" >
                    @if(auth()->user()->role === "applicant") 
                      @if(isset($userProfile['rating']->rating)) 
                        <div style="width:100%; display: flex; justify-content: center;">
                          <h4>Your rating: {{$userProfile['rating']->rating}}</h4>
                        </div>
                      @else
                        <div style="width:100%; display: flex; justify-content: center;">
                            <h4>Give this company a feedback</h4>
                        </div>
                        
                        <div style="width:100%; display: flex; justify-content: center;">
                        
                            <form class="rating">
                                <label>
                                <input type="radio" name="stars" value="1" />
                                <span class="icon">★</span>
                                </label>
                                <label>
                                <input type="radio" name="stars" value="2" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                </label>
                                <label>
                                <input type="radio" name="stars" value="3" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>   
                                </label>
                                <label>
                                <input type="radio" name="stars" value="4" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                </label>
                                <label>
                                <input type="radio" name="stars" value="5" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                </label>
                            </form>
                            
                        </div>
                        <div style="width:100%; display: flex; justify-content: center;">
                            <button id="rateBtn" class="btn btn-primary">Rate</button>
                        </div>
                      @endif  
                    @else 
                      <div style="width:100%; display: flex; justify-content: center;">
                        <h4>Avg Rating: {{$userProfile['avgRating']}}</h4>
                      </div>
                    @endif
                </div>
            </div>
    
          </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                    
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Company Name</h6>
                    </div>
                    <div class="col-sm-9">
                      {{ $userProfile['user']->name }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9">
                      {{ $userProfile['user']->email }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Contact</h6>
                    </div>
                    <div class="col-sm-9">
                      @if (isset($userProfile['user']->number))
                          {{ $userProfile['user']->number }}
                      @else
                          <h5 class="text-secondary">None</h5>
                      @endif
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9">
                      @if (isset($userProfile['user']->address))
                          {{ $userProfile['user']->address }}
                      @else
                          <h5 class="text-secondary">None</h5>
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="row gutters-sm mb-3">
           
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">About Company</h5>
                        </div>
                        <div class="card-body">
                        <h5 class="text-secondary dark-color" style="text-align: left">Overview</h5>
                            <section class="section about-section gray-bg container pt-3 mb-3" id="about">
                            
                                <h2 class="h3 block-title text-center">What we do<br><small>Whatever we do, we do with your end user in mind</small></h2>
                                     <br>
                        <div class="row pt-5 mt-30">
                            <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                                <a class="card" href="#">
                                 <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fas fa-globe fa-3x head-icon"></i></div>
                                    <div class="card-body text-center">
                                        <h3 class="card-title pt-1">Website</h3>
                                        <p class="card-text text-sm">
                                            @if (isset($userProfile['user']->website))
                                            {{ $userProfile['user']->website }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                        </p>
                                        <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                    </div>
                                </a>
                            </div>
                        <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                            <a class="card" href="#">
                                <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fa fa-industry fa-3x head-icon"></i></div>
                                <div class="card-body text-center">
                                    <h3 class="card-title pt-1">Industry</h3>
                                    <div class="card-text text-sm">
                                        @if (isset($userProfile['user']->company_industry ))
                                            {{ $userProfile['user']->company_industry }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                    </div>
                                    <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                            <a class="card" href="#">
                                <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fas fa-user-friends fa-3x head-icon"></i></div>
                                <div class="card-body text-center">
                                    <h3 class="card-title pt-1">Company Size</h3>
                                    <p class="card-text text-sm">
                                        @if (isset($userProfile['user']->company_size))
                                            {{ $userProfile['user']->company_size }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                    </p>
                                    <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                            <a class="card" href="#">
                                <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fas fa-universal-access fa-3x head-icon"></i></div>
                                <div class="card-body text-center">
                                    <h3 class="card-title pt-1">Type</h3>
                                    <p class="card-text text-sm">
                                        @if (isset($userProfile['user']->company_type))
                                            {{ $userProfile['user']->company_type }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                    </p>
                                    <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                </div>
                            </a>
                        </div>
                       
                        <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                            <a class="card" href="#">
                                <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fas fa-map-marked-alt fa-3x head-icon"></i></div>
                                <div class="card-body text-center">
                                    <h3 class="card-title pt-1">Specialties</h3>
                                    <p class="card-text text-sm">
                                        @if (isset($userProfile['user']->company_specialties))
                                            {{ $userProfile['user']->company_specialties }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                    </p>
                                    <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6 mb-30 pb-5">
                            <a class="card" href="#">
                                <div class="box-shadow bg-white rounded-circle mx-auto text-center" style="width: 90px; height: 90px; margin-top: -45px;"><i class="fas fa-map-marked-alt fa-3x head-icon"></i></div>
                                <div class="card-body text-center">
                                    <h3 class="card-title pt-1">Locations</h3>
                                    <p class="card-text text-sm">
                                        @if (isset($userProfile['user']->company_locations))
                                            {{ $userProfile['user']->company_locations }}
                                        @else
                                            <h5 class="text-secondary">None</h5>
                                        @endif 
                                    </p>
                                    <span class="text-sm text-uppercase font-weight-bold">Learn More&nbsp;<i class="fe-icon-arrow-right"></i></span>
                                </div>
                            </a>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
           
              </div>

              <div class="row gutters-sm mb-3">
           
                <div class="col-sm-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Page Posts</h5>
                        </div>
                        <div class="card-body">
                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY=" crossorigin="anonymous" />

                            <div class="filter-result">
                                @if($posts[0] !== null)
                                    @foreach($posts as $post)
                                        <div class="job-box d-md-flex align-items-center justify-content-between mb-3">
                                            <div class="job-left my-4 d-md-flex align-items-center flex-wrap">
                                                
                                                <div class="job-content">
                                                    <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}"><p class="text-center text-md-left">{{ $post->title }}</p></a>
                                                    <ul class="d-md-flex flex-wrap text-capitalize ff-open-sans">
                                                        <li class="mr-md-4">
                                                            <i class="zmdi zmdi-pin mr-2"></i> {{ $post->workCity }}
                                                        </li>
                                                        <li class="mr-md-4">
                                                            <small class="fa-solid fa-peso-sign">PhP</small> {{ $post->salaryAvg  }}
                                                        </li>
                                                        <li class="mr-md-4">
                                                            <i class="zmdi zmdi-time mr-2"></i> {{ $post->contract }}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="job-right my-4 flex-shrink-0">
                                                <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}" class="btn d-block w-100 d-sm-inline-block btn-light">View</a>
                                            </div>
                                        </div>
                                    @endforeach
                                    <!-- Pagination -->
                                    <br>
                                    {{$posts->links('vendor.pagination.custompagination')}}
                                @else 
                                <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                                @endif
                                
                                
                            </div>
                        </div>
           
                    </div>
                    <div class="row gutters-sm mb-3">
           
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">Company Ratings</h5>
                                </div>
                                <div class="card-body">
                                  
                                    <div class="graph-star-rating-header">
                                        
                                        <p class="text-black mb-4 mt-2">Rated {{$userProfile['avgRating']}} out of 5</p>
                                    </div>
                                    <div class="graph-star-rating-body">
                                        <div class="rating-list">
                                            <div class="rating-list-left text-black">
                                                5 Star
                                            </div>
                                            <div class="rating-list-center">
                                                <div class="progress">
                                                    <div style="width: {{$userProfile['percent5']}}%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                        <span class="sr-only">{{$userProfile['percent5']}}% </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rating-list-right text-black">{{$userProfile['percent5']}}%</div>
                                        </div>
                                        <div class="rating-list">
                                            <div class="rating-list-left text-black">
                                                4 Star
                                            </div>
                                            <div class="rating-list-center">
                                                <div class="progress">
                                                    <div style="width: {{$userProfile['percent4']}}% " aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                        <span class="sr-only">{{$userProfile['percent4']}}% </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rating-list-right text-black">{{$userProfile['percent4']}}% </div>
                                        </div>
                                        <div class="rating-list">
                                            <div class="rating-list-left text-black">
                                                3 Star
                                            </div>
                                            <div class="rating-list-center">
                                                <div class="progress">
                                                    <div style="width: {{$userProfile['percent3']}}% " aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                        <span class="sr-only">{{$userProfile['percent3']}}% </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rating-list-right text-black">{{$userProfile['percent3']}}% </div>
                                        </div>
                                        <div class="rating-list">
                                            <div class="rating-list-left text-black">
                                                2 Star
                                            </div>
                                            <div class="rating-list-center">
                                                <div class="progress">
                                                    <div style="width: {{$userProfile['percent2']}}% " aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                        <span class="sr-only">{{$userProfile['percent2']}}% </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rating-list-right text-black">{{$userProfile['percent2']}}% </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<script>
    var rating = 0;
    $(':radio').change(function() {
        rating = this.value;
        
    });

    $('#rateBtn').click(function(){
        var username = "{{ $userProfile['user']->username }}";
        window.location.href = username+'/rate/'+rating;
    });
</script>
    
<style>
.rating {
  display: inline-block;
  position: relative;
  height: 50px;
  line-height: 50px;
  font-size: 50px;
}

.rating label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.rating label:last-child {
  position: static;
}

.rating label:nth-child(1) {
  z-index: 5;
}

.rating label:nth-child(2) {
  z-index: 4;
}

.rating label:nth-child(3) {
  z-index: 3;
}

.rating label:nth-child(4) {
  z-index: 2;
}

.rating label:nth-child(5) {
  z-index: 1;
}

.rating label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.rating label .icon {
  float: left;
  color: transparent;
}

.rating label:last-child .icon {
  color: #a5a5a5;
}

.rating:not(:hover) label input:checked ~ .icon,
.rating:hover label:hover input ~ .icon {
  color: #09f;
}

.rating label input:focus:not(:checked) ~ .icon:last-child {
  color: #000;
  text-shadow: 0 0 5px #09f;
}

.total-like-user-main a {
    display: inline-block;
    margin: 0 -17px 0 0;
}
.total-like {
    border: 1px solid;
    border-radius: 50px;
    display: inline-block;
    font-weight: 500;
    height: 34px;
    line-height: 33px;
    padding: 0 13px;
    vertical-align: top;
}
.restaurant-detailed-ratings-and-reviews hr {
    margin: 0 -24px;
}
.graph-star-rating-header .star-rating {
    font-size: 17px;
}
.progress {
    background: #f2f4f8 none repeat scroll 0 0;
    border-radius: 0;
    height: 30px;
}
.rating-list {
    display: inline-flex;
    margin-bottom: 15px;
    width: 100%;
}
.rating-list-left {
    height: 16px;
    line-height: 29px;
    width: 10%;
}
.rating-list-center {
    width: 80%;
}
.rating-list-right {
    line-height: 29px;
    text-align: right;
    width: 10%;
}
            /* ===== Career ===== */
            .career-form {
              background-color: #fff;
              border-radius: 5px;
              padding: 0 16px;
            }
            
            .career-form .form-control {
              border: 0;
              padding: 12px 15px;
            
            }
            
            .career-form .custom-select {
              border: 0;
              padding: 12px 15px;
              width: 100%;
              border-radius: 5px;
              text-align: left;
              height: auto;
              background-image: none;
            }
            
            .career-form .custom-select:focus {
              -webkit-box-shadow: none;
                      box-shadow: none;
            }
            
            .career-form .select-container {
              position: relative;
            }
            
            .career-form .select-container:before {
              position: absolute;
              right: 15px;
              top: calc(50% - 14px);
              font-size: 18px;
              content: '\F2F9';
              font-family: "Material-Design-Iconic-Font";
            }
            
            .filter-result .job-box {
              -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
                      box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
              border-radius: 10px;
              padding: 10px 35px;
            }
            
            ul {
              list-style: none; 
            }
            
            .list-disk li {
              list-style: none;
              margin-bottom: 12px;
            }
            
            .list-disk li:last-child {
              margin-bottom: 0;
            }
            
            .job-box .img-holder {
              height: 65px;
              width: 65px;
              background-color: #2A57A3;
              font-family: "Open Sans", sans-serif;
              color: #fff;
              font-size: 22px;
              font-weight: 700;
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-pack: center;
                  -ms-flex-pack: center;
                      justify-content: center;
              -webkit-box-align: center;
                  -ms-flex-align: center;
                      align-items: center;
              border-radius: 65px;
            }
            
            .career-title {
              background-color: #2A57A3;
              color: #fff;
              padding: 15px;
              text-align: center;
              border-radius: 10px 10px 0 0;
              background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
              background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
            }
            
            .job-overview {
              -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
                      box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
              border-radius: 10px;
            }
            
            @media (min-width: 992px) {
              .job-overview {
                position: -webkit-sticky;
                position: sticky;
                top: 70px;
              }
            }
            
            .job-overview .job-detail ul {
              margin-bottom: 28px;
            }
            
            .job-overview .job-detail ul li {
              opacity: 0.75;
              font-weight: 600;
              margin-bottom: 15px;
            }
            
            .job-overview .job-detail ul li i {
              font-size: 20px;
              position: relative;
              top: 1px;
            }
            
            .job-overview .overview-bottom,
            .job-overview .overview-top {
              padding: 35px;
            }
            
            .job-content ul li {
              border-bottom: 1px solid #ccc;
              padding: 10px 5px;
            }
            
            @media (min-width: 768px) {
              .job-content ul li {
                border-bottom: 0;
                padding: 0;
              }
            }
            
            .job-content ul li i {
              font-size: 20px;
              position: relative;
              top: 1px;
            }
            
                    </style>
@endsection