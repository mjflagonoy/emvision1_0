@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1573035860/Pra/Hadie-profile-pic-circle-1.png" alt="Admin" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4>{{ $userProfile['user']->name }}</h4>
                      <p class="text-muted font-size-sm">
                        @if (isset($userProfile['user']->biography))
                            {{ $userProfile['user']->biography }}
                        @else
                            <h5 class="text-secondary">None</h5>
                        @endif  
                      </p>

                      @auth
                        @if ($userProfile['user']->username == auth()->user()->username)
                          <a class="btn btn-primary" href="{{ route('profile/edit') }}">Edit Profile</a>    
                        @endif 
                      @endauth
                      
                    </div>
                  </div>
                </div>
              </div>

              
    
              <div class="card mt-3">
                <div class="card-header">
                  <h5 class="card-title mb-0">Skills</h5>
                </div>
                <div class="card-body">
                   @if (isset($userProfile['skills'][0]) )
                       
                       @foreach ($userProfile['skills'] as $skill)
                        <h5>{{ $skill->skill  }}</h5>
                        <div class="progress mb-3" style="height: 15px">
                            <div class="progress-bar bg-primary" role="progressbar" style="width: {{ $skill->proficiency }}%" aria-valuenow="{{ $skill->proficiency }}" aria-valuemin="0" aria-valuemax="100">{{ $skill->proficiency }}%</div>
                        </div>
                      @endforeach
                   @else
                      <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                   @endif  
                </div>
              </div>

              
          </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9">
                      {{ $userProfile['user']->name }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9">
                      {{ $userProfile['user']->email }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Contact</h6>
                    </div>
                    <div class="col-sm-9">
                      @if (isset($userProfile['user']->number))
                          {{ $userProfile['user']->number }}
                      @else
                          <h5 class="text-secondary">None</h5>
                      @endif
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9">
                      @if (isset($userProfile['user']->address))
                          {{ $userProfile['user']->address }}
                      @else
                          <h5 class="text-secondary">None</h5>
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="row gutters-sm mb-3">
           
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Educational Background</h5>
                        </div>
                        <div class="card-body">
                          @if (isset($userProfile['educations'][0]) )
                              <ul class="timeline">
                              @foreach ($userProfile['educations'] as $education)
                                <li>
                                  <p style="display: inline" class="text-primary" >{{ $education->school }}</p>
                                  <p style="display: inline" class="text-primary float-right" >{{ $education->yearGraduation }}</p>
                                  <h5>{{ $education->field }}</h5>
                                  <h5 class="text-secondary">{{ $education->level }}</h5>
                                </li>
                              @endforeach
                              </ul>
                          @else
                              <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                          @endif     
                        </div>
                    </div>
                </div>
           
              </div>

              <div class="row gutters-sm mb-3">
           
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Work Experiences</h5>
                        </div>
                        <div class="card-body">
                          @if (isset($userProfile['experiences'][0]) )
                            <ul class="timeline">
                              @foreach ($userProfile['experiences'] as $experience)
                                <li>
                                  <p style="display: inline" class="text-primary" >{{ $experience->company }}</p>
                                  <p style="display: inline" class="text-primary float-right">{{ $experience->yearEmployment }}</p>
                                  <h5>{{ $experience->position}}</h5>
                                  <h5 class="text-secondary">{{ $experience->location }}</h5>
                                </li>
                              @endforeach
                            </ul>
                          @else
                              <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                          @endif   
                        </div>
                    </div>
                </div>
           
              </div>

              <div class="row gutters-sm mb-3">
           
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Achievements and Awards</h5>
                        </div>
                        <div class="card-body">
                          @if (isset($userProfile['awards'][0]) )
                            <ul class="timeline">
                              @foreach ($userProfile['awards'] as $award)
                                <li>
                                  <p style="display: inline" class="text-primary">{{ $award->award }}</p>
                                  <p style="display: inline" class="text-primary float-right" >{{ $award->awardYear }}</p>
                                  <h5>{{ $award->certifyingBody }}</h5>
                            
                                </li>
                              @endforeach
                            </ul>
                          @else
                              <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>
                          @endif
                        </div>
                    </div>
                </div>
           
              </div>

            </div>
          </div>

        </div>
    </div>

    <style>
 ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
    </style>
@endsection