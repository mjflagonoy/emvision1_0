@extends('layouts.app')

@section('content')
  <div class="card mx-auto" style="width: 25rem;">
    <div class="card-body">
    

        <div class="font-weight-bold text-center mb-3">Account Registration</div>
        <form action="{{ route('register') }}" method="post">
            @csrf
            <div class="form-group">
              <input onblur='talk("name")' onclick='listen("name")' type="name" class="form-control @error('name') border border-danger @enderror" id="name" name="name" placeholder="Enter name" value="{{ old('name') }}">
              @error('name')
                <small class="form-text text-danger">{{ $message }}</small>
              @enderror
            </div>
			
            <div class="form-group">
              <input onblur='talk("email")' onclick='listen("email")' type="email" class="form-control @error('email') border border-danger @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
              
              @error('email')
                <small class="form-text text-danger">{{ $message }}</small>
              @enderror
            </div>
            <div class="form-group">
              
                <input onblur='talk("username")' onclick='listen("username")' type="text" class="form-control @error('username') border border-danger @enderror" id="username" name="username" placeholder="Enter username" value="{{ old('username') }}">
               
                @error('username')
                 <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
          
              <input onclick='listen("password")' type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password" placeholder="Choose a password">

              @error('password')
                 <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
              
                <input onclick='listen("password_confirmation")' type="password" class="form-control @error('password_confirmation') border border-danger @enderror" id="password_confirmation" name="password_confirmation" placeholder="Repeat your password">
             
                @error('password_confirmation')
                 <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="mb-3 text-center">
				<button type="submit" class="btn">Register</button>
			</div>
			
			<h4 class="text-center"> OR </h4>
			<div class="text-center justify-end mt-3">
             <a href="{{URL::to('auth/google')}}">
                <img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png">
             </a>
          </form>
    </div>
  </div>
  <script>
    

    $("#password_confirmation").on("blur",  function() {
      var pass1 = document.getElementById("password").value;
      var pass2 = document.getElementById("password_confirmation").value;

      if(pass1 !== pass2){
        beep();
      }
    });

    
  </script>
  
@endsection