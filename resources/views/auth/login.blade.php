@extends('layouts.app')

@section('content')
  <div class="card mx-auto" style="width: 25rem;">
    <div class="card-body">
      
        <div class="font-weight-bold text-center mb-3">Account Login</div>
        @if(session('status'))
        <small class="form-text text-danger">{{ session('status') }}</small>
        @endif
        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="form-group">
              <input onclick='listen("test1")' type="email" class="final form-control @error('email') border border-danger @enderror" id="test1" onclick="emailInput(event)" name="email" placeholder="Enter email">
              @error('email')
                <small class="form-text text-danger">{{ $message }}</small>
              @enderror
            </div><span id="interim_span" class="interim"></span>
            <div class="form-group">
              <input onclick='listen("test2")' type="password" class="form-control @error('password') border border-danger @enderror" id="test2" onclick="passwordInput(event)"name="password" placeholder="Enter password"><span id="interim_span"></span>
              @error('password')
                 <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
              <input onclick='listen("test3")' type="password" class="form-control @error('password') border border-danger @enderror" id="test3" onclick="passwordInput(event)"name="password2" placeholder="Reenter password"><span id="interim_span"></span>
              
            </div>
            <div class="form-check mb-3 text-center">
               <input type="checkbox" id="rememberme" name="rememberme">
               <label class="form-check-label text-left pr-4" for="rememberme" style="font-size: 14pt;">Remember me</label>
			   <button type="submit" class="btn">Login</button>
            </div>
			
			<h4 class="text-center"> OR </h4>
			<div class="text-center justify-end mt-3">
             <a href="{{ url('auth/google') }}">
                <img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png">
             </a>
          </div>
		
          </form>
		  
    </div>
  </div>

  <script>
    $("#test1").on("blur",  function() {
      talk("test1");
    });

    $("#test3").on("blur",  function() {
      var pass1 = document.getElementById("test2").value;
      var pass2 = document.getElementById("test3").value;

      if(pass1 !== pass2){
        beep();
      }
    });

    
  </script>
@endsection