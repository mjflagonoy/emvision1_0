
@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js" integrity="sha512-VvWznBcyBJK71YKEKDMpZ0pCVxjNuKwApp4zLF3ul+CiflQi6aIJR+aZCP/qWsoFBA28avL5T5HA+RE+zrGQYg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-angular.min.js" integrity="sha512-KT0oYlhnDf0XQfjuCS/QIw4sjTHdkefv8rOJY5HHdNEZ6AmOh1DW/ZdSqpipe+2AEXym5D0khNu95Mtmw9VNKg==" crossorigin="anonymous"></script>
    <style type="text/css">
        .bootstrap-tagsinput{
            width: 100%;
        }
        .label-info{
            background-color: #17a2b8;

        }
        .label {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,
            border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
    </style>
<div class="container">

      <div class="container-fluid col-md-8">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}">{{ $post->title }}</a>
            </li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>

         
          <!-- DataTables Example -->
          <div class="flex-md-row justify-content-center">
                    @if(Session::has('success'))
                      <small class="text-success">{{session('success')}}</small>
                    @endif
               
              
              <form method="post" action="{{url('company/update-post')}}" enctype="multipart/form-data">            
                @csrf
                <input value="{{ $post->id }}" hidden name="id">
                <div class="card mb-3 ">
                  <div class="card-body "> 
                    <h4 class="font-weight-light"><span class="icon-briefcase"></span> Job Overview</h4>
                    <div class="p-2">
                        <div class="mb-3"> 
                          <h5 class="font-weight-bold">What's the Job Title?<span class="text-danger">*</span></h5>
                          <input value="{{ $post->title }}" type="text" name="title" class="form-control" placeholder="Enter a job title"/>
                          @error('title')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>

                        <div class="mb-3"> 
                          <h5 class="font-weight-bold">What's the category this job belongs to?<span class="text-danger">*</span></h5>
                          <select class="form-control" name="category">
                            <option selected disabled>{{ $post->category->title }}</option>
                            @foreach($cats as $cat)
                            <option value="{{$cat->id}}">{{$cat->title}}</option>
                            @endforeach
                          </select>
                          @error('category')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>

                        <div class="mb-3"> 
                          <h5 class="font-weight-bold">What are the required years of experience?</h5>
                      
                          <input value="{{ $post->requiredExperience }}" type="number" name="requiredExperience" class="form-control" placeholder="Minimum work experience"/>
                        </div>

                        <div class="mb-3"> 
                          <h5 class="font-weight-bold">Your job overview<span class="text-danger">*</span></h5>
                          
                          <textarea class="ckeditor" name="detail" id="detail" placeholder="Short detail about the job" rows="5"></textarea>
                          @error('error')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                          
                        </div>
                        
                    </div>
                  </div>
                </div>

                <div class="card mb-3 ">
                  <div class="card-body "> 
                    <h4 class="font-weight-light"><span class="icon-file"></span> Contract</h4>
                    <div class="p-2">
                      <div class="mb-3"> 
                        <h5 class="font-weight-bold">What type of contract is it?<span class="text-danger">*</span></h5>
                        <div class="form-check">
                          <input @if($post->contract === "Full-time") checked @endif class="form-check-input" type="radio" name="contract" value="Full-time" id="flexRadioDefault1">
                          <h5>
                            <label class="form-check-label" for="flexRadioDefault1">
                              Full-time
                            </label>
                          </h5>
                        </div>
                        <div class="form-check">
                          <input @if($post->contract === "Part-time") checked @endif class="form-check-input" type="radio" name="contract" value="Part-time" id="flexRadioDefault2">
                          <h5>
                            <label class="form-check-label" for="flexRadioDefault2">
                              Part-time
                            </label>
                          </h5>
                        </div>
                        <div class="form-check">
                          <input @if($post->contract === "Contract") checked @endif class="form-check-input" type="radio" name="contract" value="Contract" id="flexRadioDefault2">
                          <h5>
                            <label class="form-check-label" for="flexRadioDefault2">
                              Contract
                            </label>
                          </h5>
                        </div>
                        <div class="form-check">
                          <input @if($post->contract === "Temporary") checked @endif  class="form-check-input" type="radio" name="contract" value="Temporary" id="flexRadioDefault2">
                          <h5>
                            <label class="form-check-label" for="flexRadioDefault2">
                              Temporary
                            </label>
                          </h5>
                        </div>
                        <div class="form-check">
                          <input @if($post->contract === "Internship") checked @endif class="form-check-input" type="radio" name="contract" value="Internship" id="flexRadioDefault2">
                          <h5>
                            <label class="form-check-label" for="flexRadioDefault2">
                              Internship
                            </label>
                          </h5>
                        </div>
                        @error('contract')
                          <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                      <div class="mb-3"> 
                        <h5 class="font-weight-bold">What's the position level?<span class="text-danger">*</span></h5>
                        <select name="position" id="" class="form-control">
                            <option selected disabled>{{ $post->position }}</option>
                            <option>Entry-level</option>
                            <option>Intermediate</option>
                            <option>Mid-level</option>
                            <option>Senior or executive-level</option>
                        </select>
                        @error('position')
                          <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mb-3 ">
                  <div class="card-body "> 
                    <h4 class="font-weight-light"><span class="icon-location-arrow"></span> Work Location</h4>
                    <div class="p-2">
                        <h5 class="font-weight-bold">Where is your new employee going to work?<span class="text-danger">*</span></h5>
                        
                        <h6><span class="font-weight-light">Select Region</span></h6>
                        <select id="region" class="form-control mb-3" name="workRegion"></select>
                        @error('workRegion')
                          <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <h6><span class="font-weight-light">Select Province</span></h6>
                        <select id="province" class="form-control mb-3" name="workProvince"></select>
                        @error('workProvince')
                          <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                        <h6><span class="font-weight-light">Select City</span></h6>
                        <select id="city" class="form-control mb-3" name="workCity"></select>
                        @error('workCity')
                          <small class="form-text text-danger">{{ $message }}</small>
                        @enderror

                        <input value="{{ $post->workRegion }}" hidden name="workRegionName" id="workRegionName" >
                        <input value="{{ $post->workProvince }}" hidden name="workProvinceName" id="workProvinceName">
                        <input value="{{ $post->workCity }}" hidden name="workCityName" id="workCityName">
                    </div>
                  </div>
                </div>

                <div class="card mb-3 ">
                  <div class="card-body "> 
                    <h4 class="font-weight-light"><span class="icon-usd"></span> Budget</h4>
                    <div class="p-2">
                        <h5 class="font-weight-bold">What's the monthly salary range?<span class="font-weight-light"> <small>( using Philippine Peso currency )</small></span></h5>
                        <input value="{{ $post->salaryMin }}" type="number" class="form-control" name="salaryMin" placeholder="Min">
                        <small>to<small>
                        <input value="{{ $post->salaryMax }}" type="number" class="form-control" name="salaryMax" placeholder="Max">
                    </div>
                  </div>
                </div>

                <div class="card mb-3 ">
                  <div class="card-body "> 
                    <h4 class="font-weight-light"><span class="fas fa-hashtag"></span> Tags</h4>
                    <div class="p-2">
                        <h5 class="font-weight-bold">Provide relevant keywords for this job <span class="font-weight-light"> <small>( Press the enter key for every tag )</small></span></h5>
                        
                        <input value="{{ $post->tags }}" type="text" data-role="tagsinput" name="tags" class="form-control">
                    </div>
                  </div>
                  
                </div>

                <input type="submit" class="form-control btn btn-primary mb-5" value="Save">
              </form>
          </div>
      </div>
          

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js"></script>
        <script src="https://cdn.ckeditor.com/4.17.1/basic/ckeditor.js"></script>
        
          <script>

                $(document).ready(function()
                {
                    var msg = "{!! $post->detail !!}";
                    CKEDITOR.instances.detail.setData(msg);
                    // var element = CKEDITOR.dom.element.createFromHtml( msg );
                    // CKEDITOR.instances.detail.insertElement( element );
                });
                
                var my_handlers = {

                fill_provinces:  function(){
                    fillLocationName('region', 'workRegionName');

                    var region_code = $(this).val();
                    $('#province').ph_locations('fetch_list', [{"region_code": region_code}]);
                    
                    fillLocationName('province', 'workProvinceName');
                },

                fill_cities: function(){
                    fillLocationName('province', 'workProvinceName');

                    var province_code = $(this).val();
                    $('#city').ph_locations( 'fetch_list', [{"province_code": province_code}]);
      
                    fillLocationName('city', 'workCityName');
                },


                fill_barangays: function(){
                    fillLocationName('city', 'workCityName');
                },

              
            };
            
            function fillLocationName($locationId, $locationName){
                var select_temp = document.getElementById($locationId);
                var selected_caption = select_temp.options[select_temp.selectedIndex].text;
                  
                document.getElementById($locationName).value = selected_caption;
            }

            $(function(){
                $('#region').on('change', my_handlers.fill_provinces);
                $('#province').on('change', my_handlers.fill_cities);
                $('#city').on('change', my_handlers.fill_barangays);

                $('#region').ph_locations({'location_type': 'regions'});
                $('#province').ph_locations({'location_type': 'provinces'});
                $('#city').ph_locations({'location_type': 'cities'});
                $('#barangay').ph_locations({'location_type': 'barangays'});

                $('#region').ph_locations('fetch_list');
            });
        </script>

        <script src="https://cdn.ckeditor.com/4.17.1/basic/ckeditor.js"></script>
        
@endsection

      