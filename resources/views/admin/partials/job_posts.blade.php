<div class="tab-pane fade" id="jobs" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Job Posts</h5>
        </div>
        <div class="card-body">
            @if ($posts !== null)
                <div class="user-dashboard-info-box table-responsive mb-0 bg-white">
                    <form method="post" action="">
                        @csrf
                        <table class="table manage-candidates-top mb-0">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th class="text-center">Status</th>
                                    <th class="action text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <span hidden></span>
                                    @foreach($posts as $post)
                                        <tr class="candidates-list">
                                            <td class="title">
                                                <div class="thumb">
                                                    {{-- <img class="img-fluid" src="https://bootdey.com/img/Content/avatar/avatar7.png" alt=""> --}}
                                                </div>
                                                <div class="candidate-list-details">
                                                    <div class="candidate-list-info">
                                                    <div class="candidate-list-title">
                                                        <h5 class="mb-0"><a href="{{url('detail/'.Str::slug($post->title).'/'.$post->id)}}">{{ $post->title }}</a></h5>
                                                    </div>
                                                    <div class="candidate-list-option">
                                                        <ul class="list-unstyled">
                                                        <li><i class="fas fa-users pr-1"></i>{{$post->countApplicants()}} applicants</li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="candidate-list-favourite-time text-center">
                                                @if($post->status === 1)
                                                    <span class="candidate-list-time order-1 mr-2 badge badge-success"><small>Active</small></span>
                                                @else 
                                                    <span class="candidate-list-time order-1 mr-2 badge badge-dark"><small>Closed</small></span>
                                                @endif
                                            </td>
                                            <td>
                                                <ul class="list-unstyled mb-0 d-flex justify-content-end">
                                                    <li  onclick=""><a href="#" class="text-primary" data-toggle="tooltip" title="" data-original-title="view"><i class="fa fa-eye"></i></a></li>
                                                    <li class="text-primary" data-toggle="tooltip" title="" data-original-title="view">
                                                        <select id="{{"new_action".$post->id}}"  onchange="statusListener({{$post}})" class="form-control btn-primary ml-2">
                                                            <option selected disabled>Action</option>
                                                            <option value="Edit">Edit</option>
                                                            @if($post->status === 1)
                                                                <option value="Close">Close</option>
                                                            @else 
                                                                <option value="Open">Open</option>
                                                            @endif
                                                        </select>
                                                    </li>
                                                </ul>
                                                
                                            </td>
                                        </tr>
                                    @endforeach  
                            </tbody>
                        </table>
                        <div class="text-center mt-3 mt-sm-3">
                            <!-- Pagination -->
                            {{$posts->links('vendor.pagination.custompagination')}}
                        </div>
                        {{-- <input name="submit" type="submit" class="btn btn-primary" value="Save Changes"> --}}
                    </form>
                    
                </div>  
            @else
                <h5 class="text-secondary" style="text-align: center">Nothing to show</h5>  
            @endif  

                                  
        </div>
    </div>
</div>


<script>
    function statusListener(post){
        var e = document.getElementById("new_action"+post.id);
        var strStatus = e.options[e.selectedIndex].text;
        if(strStatus === "Close"){
            window.location.href = "detail/" + post.title + "/" + post.id + "/update-status/0";
            
        }
        else if(strStatus === "Open"){
            window.location.href = "detail/" + post.title + "/" + post.id + "/update-status/1";
            
        }
        else if(strStatus === "Edit"){
            window.location.href = "detail/" + post.title + "/" + post.id + "/edit";
        }
    }
</script>
<script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
        
        
<style>

.close-modal{
    background-color: transparent;
    background-repeat: no-repeat;
    border: none;
    cursor: pointer;
    overflow: hidden;
    outline: none;

}
/* user-dashboard-info-box */
.user-dashboard-info-box .candidates-list .thumb {
    margin-right: 20px;
}
.user-dashboard-info-box .candidates-list .thumb img {
    width: 80px;
    height: 80px;
    -o-object-fit: cover;
    object-fit: cover;
    overflow: hidden;
    border-radius: 50%;
}

.user-dashboard-info-box .title {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 30px 0;
}

.user-dashboard-info-box .candidates-list td {
    vertical-align: middle;
}

.user-dashboard-info-box td li {
    margin: 0 4px;
}

.user-dashboard-info-box .table thead th {
    border-bottom: none;
}

.table.manage-candidates-top th {
    border: 0;
}

.user-dashboard-info-box .candidate-list-favourite-time .candidate-list-favourite {
    margin-bottom: 10px;
}

.table.manage-candidates-top {
    min-width: 650px;
}

.user-dashboard-info-box .candidate-list-details ul {
    color: #969696;
}

/* Candidate List */
.candidate-list {
    background: #ffffff;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    border-bottom: 1px solid #eeeeee;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 20px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
.candidate-list:hover {
    -webkit-box-shadow: 0px 0px 34px 4px rgba(33, 37, 41, 0.06);
    box-shadow: 0px 0px 34px 4px rgba(33, 37, 41, 0.06);
    position: relative;
    z-index: 99;
}
.candidate-list:hover a.candidate-list-favourite {
    color: #e74c3c;
    -webkit-box-shadow: -1px 4px 10px 1px rgba(24, 111, 201, 0.1);
    box-shadow: -1px 4px 10px 1px rgba(24, 111, 201, 0.1);
}

.candidate-list .candidate-list-image {
    margin-right: 25px;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 80px;
    flex: 0 0 80px;
    border: none;
}
.candidate-list .candidate-list-image img {
    width: 80px;
    height: 80px;
    -o-object-fit: cover;
    object-fit: cover;
}

.candidate-list-title {
    margin-bottom: 5px;
}

.candidate-list-details ul {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-bottom: 0px;
}
.candidate-list-details ul li {
    margin: 5px 10px 5px 0px;
    font-size: 13px;
}

.candidate-list .candidate-list-favourite-time {
    margin-left: auto;
    text-align: center;
    font-size: 13px;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 90px;
    flex: 0 0 90px;
}
.candidate-list .candidate-list-favourite-time span {
    display: block;
    margin: 0 auto;
}
.candidate-list .candidate-list-favourite-time .candidate-list-favourite {
    display: inline-block;
    position: relative;
    height: 40px;
    width: 40px;
    line-height: 40px;
    border: 1px solid #eeeeee;
    border-radius: 100%;
    text-align: center;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    margin-bottom: 20px;
    font-size: 16px;
    color: #646f79;
}
.candidate-list .candidate-list-favourite-time .candidate-list-favourite:hover {
    background: #ffffff;
    color: #e74c3c;
}

.candidate-banner .candidate-list:hover {
    position: inherit;
    -webkit-box-shadow: inherit;
    box-shadow: inherit;
    z-index: inherit;
}

.bg-white {
    background-color: #ffffff !important;
}
</style>

