<div class="tab-pane fade" id="adduser" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Add User</h5>
        </div>
        <div class="card-body">
            <form action="{{ route('/admin/add-user') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="role">Role of User</label>
                    <select id="role" name="role" class="form-control">
                      <option selected>admin</option>
                      <option>employer</option>
                      <option>applicant</option>
                    </select>
                    @error('role')
                        <small class="form-text text-danger">{{ $message }}</small>
                      @enderror
                  </div>
                  
                    <div class="form-group">
                      <input  type="name" class="form-control @error('name') border border-danger @enderror" id="name" name="name" placeholder="Enter name" value="{{ old('name') }}">
                      @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                    
                    <div class="form-group">
                      <input  type="email" class="form-control @error('email') border border-danger @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                      
                      @error('email')
                        <small class="form-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                    <div class="form-group">
                      
                        <input type="text" class="form-control @error('username') border border-danger @enderror" id="username" name="username" placeholder="Enter username" value="{{ old('username') }}">
                       
                        @error('username')
                         <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                  
                      <input type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password" placeholder="Choose a password">
        
                      @error('password')
                         <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
              
                        <input onclick='listen("password_confirmation")' type="password" class="form-control @error('password_confirmation') border border-danger @enderror" id="password_confirmation" name="password_confirmation" placeholder="Repeat your password">
                     
                        @error('password_confirmation')
                         <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    
                    <div class="mb-3 text-center">
                        <button type="submit" class="btn">Add User</button>
                    </div>
              </form>  

                                  
        </div>
    </div>
</div>

        
