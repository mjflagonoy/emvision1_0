@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card" >
        <div class="card-header">
            <h4>Edit Admin Info<h4>
        </div>
        <div class="card-body">
            <div style="width:100%; display: flex; justify-content: center;">
                
                @if(Session::has('success'))
                    <p class="text-success">{{session('success')}}</p>
                @endif
                @if(Session::has('error'))
                    <p class="text-danger">{{session('error')}}</p>
                @endif
            </div>
            <div style="width:100%; display: flex; justify-content: center;">
                
                <form action="{{ route('/admin/edit-info') }}" method="post" class="col-md-6">
                    @csrf              
                        <div class="form-group">
                          <input value="{{auth()->user()->name}}" type="name" class="form-control @error('name') border border-danger @enderror" id="name" name="name" placeholder="Enter name" value="{{ old('name') }}">
                          @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                        
                        <div class="form-group">
                          <input value="{{auth()->user()->email}}" type="email" class="form-control @error('email') border border-danger @enderror" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                          
                          @error('email')
                            <small class="form-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                        <div class="form-group">
                          
                            <input value="{{auth()->user()->username}}" type="text" class="form-control @error('username') border border-danger @enderror" id="username" name="username" placeholder="Enter username" value="{{ old('username') }}">
                           
                            @error('username')
                             <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                      
                          <input type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password" placeholder="New password">
            
                          @error('password')
                             <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                  
                            <input type="password" class="form-control @error('password_confirmation') border border-danger @enderror" id="password_confirmation" name="password_confirmation" placeholder="Repeat new password">
                         
                            @error('password_confirmation')
                             <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="currentp">Enter current password to continue</label>
                            <input  type="password" class="form-control @error('currentp') border border-danger @enderror" id="currentp" name="currentp" placeholder="Current password">
                         
                            @error('currentp')
                             <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        
                        <div class="mb-3 text-center">
                            <button type="submit" class="btn">Save</button>
                        </div>
                  </form>
            </div>
            
        </div>
      </div>
</div>
@endsection