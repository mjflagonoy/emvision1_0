@extends('layouts.app')

@section('content')


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<div class="container p-0">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="{{ url('jobs') }}">Admin</a>
        </li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
      <!-- /Breadcrumb -->

    <div class="row">
        
        <div class="col-md-5 col-xl-4">
            
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Job Details</h5>
                </div>

                <div class="list-group list-group-flush" role="tablist">

                    <script>
                      $( document ).ready(function() {
                        $('#job-tab').trigger("click");
                      });
                      
                    </script>
                    <a class="list-group-item list-group-item-action" data-toggle="list" id="job-tab" href="#jobs" role="tab">
                      Job Posts
                    </a>
                    
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#ratings" role="tab">
                      Companies
                    </a>

                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#jobseekers" role="tab">
                      Job Seekers
                    </a>
                    
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#admins" role="tab">
                      Admins
                    </a>
                   
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#adduser" role="tab">
                      Add User
                    </a>

                </div>
            </div>
        </div>

        <div class="col-md-7 col-xl-8">
            @if(Session::has('success'))
                <p class="text-success">{{session('success')}}</p>
            @endif
            @if(Session::has('error'))
                <p class="text-danger">{{session('error')}}</p>
            @endif
            <div class="tab-content mb-5">
              @include('admin.partials.job_posts')

              @include('admin.partials.ratings')

              @include('admin.partials.jobseekers')

              @include('admin.partials.admins')
  
              @include('admin.partials.adduser')
            </div>
        </div>
    </div>

</div>



@endsection