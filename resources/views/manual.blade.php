@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<div class="container">
<div class="card">
<div class="card-body" >
<div class="container bootstrap snippets bootdey">
<section id="content" class="current">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <h2>EmVision User Manual</h2>
                <h2 style="font-size: 60px;line-height: 60px;margin-bottom: 20px;font-weight: 900;">With <span class="highlight">accessibility Features</span></h2>
                <p>All the features you need to navigate the website easily.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-1 col-sm-12 col-md-12 col-lg-10">
                <div class="features-list" style="padding-left: 5em">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjciIGhlaWdodD0iMjAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMTguNDQ3IDE5VjEuNjg3aC02Ljc0MVYwSDI3djEuNjg3aC02Ljc0VjE5eiIvPjxwYXRoIHN0cm9rZT0iIzAwMCIgc3Ryb2tlLXdpZHRoPSIuNTI3IiBkPSJNNC4wMzMgMTl2LTcuNjE4SDF2LS43NDJoNi44ODJ2Ljc0Mkg0Ljg1VjE5eiIvPjwvZz48L3N2Zz4="
                                    alt=""
                                    aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Increase Text Size
                                </div>
                                <div class="text">Cycles on-screen texts through four different levels of text-size increases. Makes every part of your site instantly more readable.</div>
                                
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzEiIGhlaWdodD0iMTMiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjEuNSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2UtbGluZWNhcD0ic3F1YXJlIj48cGF0aCBzdHJva2UtZGFzaGFycmF5PSIzLDMuNzUiIGQ9Ik00IDYuNWgyNCIvPjxwYXRoIGQ9Ik03IDExTDIgNi41IDcgMm0xNyA5bDUtNC41TDI0IDIiLz48L2c+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Text Spacing
                                </div>
                                <div class="text">Modify text and line spacing for improved readability for dyslexic and visually impaired users. Offers three degrees of adjustments for a personalized and more accessible reading experience.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggc3Ryb2tlLXdpZHRoPSIxLjUiIGQ9Ik0xMi42MDYgOS41MjRsNy4yNCA3LjMyOC0zLjM3OSAzLjQyLTcuMjQtNy4zMjgtMy4wOCA2Ljk3Mi00Ljk0LTE4LjUxIDE4LjI4OCA1LjAwMXoiIHN0cm9rZT0iIzAwMCIgZmlsbD0ibm9uZSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Large Cursor
                                </div>
                                <div class="text">Increases the standard cursor size by 400% to ensure the pointer always remains in sight. Allows for faster and more accessible navigation through hyperlinks, tabs, and form elements.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMTMiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE2LjI3MyAxaDMuMjcyYzEuNDQ3IDAgMi44MzQuNTYyIDMuODU3IDEuNTYyQTUuMjc0IDUuMjc0IDAgMDEyNSA2LjMzM2MwIDIuOTQ2LTIuNDQyIDUuMzM0LTUuNDU1IDUuMzM0aC0zLjI3Mm0tNi41NDYgMEg2LjQ1NWE1LjUxOCA1LjUxOCAwIDAxLTMuODU3LTEuNTYyQTUuMjc0IDUuMjc0IDAgMDExIDYuMzMzQzEgMy4zODggMy40NDIgMSA2LjQ1NSAxaDMuMjcyTTguNjM2IDYuMzMzaDguNzI4IiBzdHJva2Utd2lkdGg9IjEuNSIgc3Ryb2tlPSIjMDAwIiBmaWxsPSJub25lIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Highlight Links
                                </div>
                                <div class="text">Emphasize links, buttons, and other interactive elements in clear, high-contrast colors that are easy to identify. Highlights clickable items for easier site usability, navigation, and overall accessibility.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMTMiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE2LjI3MyAxaDMuMjcyYzEuNDQ3IDAgMi44MzQuNTYyIDMuODU3IDEuNTYyQTUuMjc0IDUuMjc0IDAgMDEyNSA2LjMzM2MwIDIuOTQ2LTIuNDQyIDUuMzM0LTUuNDU1IDUuMzM0aC0zLjI3Mm0tNi41NDYgMEg2LjQ1NWE1LjUxOCA1LjUxOCAwIDAxLTMuODU3LTEuNTYyQTUuMjc0IDUuMjc0IDAgMDExIDYuMzMzQzEgMy4zODggMy40NDIgMSA2LjQ1NSAxaDMuMjcyTTguNjM2IDYuMzMzaDguNzI4IiBzdHJva2Utd2lkdGg9IjEuNSIgc3Ryb2tlPSIjMDAwIiBmaWxsPSJub25lIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Highlight Links
                                </div>
                                <div class="text">Emphasize links, buttons, and other interactive elements in clear, high-contrast colors that are easy to identify. Highlights clickable items for easier site usability, navigation, and overall accessibility.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzNiIgaGVpZ2h0PSIzNiI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMCAwaDM2djM2SDB6Ii8+PHBhdGggZD0iTTE4IDR2NS42bTAgMTYuOFYzMk04LjEwMiA4LjEwMmwzLjk2MiAzLjk2Mm0xMS44NzIgMTEuODcybDMuOTYyIDMuOTYyTTQgMThoNS42bTE2LjggMEgzMk04LjEwMiAyNy44OThsMy45NjItMy45NjJtMTEuODcyLTExLjg3MmwzLjk2Mi0zLjk2MiIgc3Ryb2tlPSIjMDAwIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS13aWR0aD0iMiIvPjwvZz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Pause Animations
                                </div>
                                <div class="text">Pauses content that moves or auto-updates that is considered an accessibility barrier. Stops animations, blinking, and flashing content that distracts and may trigger seizures.</div>
                                
                            </div>
                        </div>
                        

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgc3Ryb2tlPSIjMDAwIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxyZWN0IHN0cm9rZS13aWR0aD0iMS41IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHdpZHRoPSIyNCIgaGVpZ2h0PSIxNi44IiByeD0iMS41Ii8+PGNpcmNsZSBzdHJva2Utd2lkdGg9Ii43NSIgZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJub256ZXJvIiBjeD0iMTIiIGN5PSI4LjI1IiByPSIzIi8+PHBhdGggc3Ryb2tlLXdpZHRoPSIxLjUiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgZD0iTTcuMiAyMS42aDkuNk0xMiAxNi44djQuOCIvPjwvZz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Light Contrast
                                </div>
                                <div class="text">Boost the contrast of all site elements with a high contrast light color theme. One-click remediation to inaccessible and non-compliant low contrast elements, texts, buttons &amp; form fields.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgc3Ryb2tlPSIjMDAwIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxyZWN0IHN0cm9rZS13aWR0aD0iMS41IiBmaWxsPSIjMDAwIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHdpZHRoPSIyNCIgaGVpZ2h0PSIxNi42MTkiIHJ4PSIxLjUiLz48ZWxsaXBzZSBzdHJva2Utd2lkdGg9Ii45MzgiIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyIgY3g9IjEyIiBjeT0iOC4xNjEiIHJ4PSIzLjc1IiByeT0iMy43MSIvPjxwYXRoIHN0cm9rZS13aWR0aD0iMS41IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIGQ9Ik02Ljc1IDIxLjUxNmg5LjZNMTIgMTYuMzIzdjQuNzQ4Ii8+PC9nPjwvc3ZnPg=="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Dark Contrast
                                </div>
                                <div class="text">Boost the contrast of all site elements with a high contrast dark color theme. One-click remediation to inaccessible and non-compliant low contrast elements, texts, buttons and form fields.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cmVjdCBzdHJva2U9IiMwMDAiIHN0cm9rZS13aWR0aD0iMS41IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHdpZHRoPSIyNCIgaGVpZ2h0PSIxNi44IiByeD0iMS41Ii8+PHBhdGggZD0iTTkuNjE0IDEwLjYzNmEzLjM3NSAzLjM3NSAwIDExNC43NzItNC43NzIgMy4zNzUgMy4zNzUgMCAwMS00Ljc3MiA0Ljc3MnoiIGZpbGw9IiMwMDAiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIHN0cm9rZT0iIzAwMCIgc3Ryb2tlLXdpZHRoPSIxLjUiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgZD0iTTcuMiAyMS42aDkuNk0xMiAxNi44djQuOCIvPjxwYXRoIGQ9Ik0xLjE4IDE1Ljk3NEwyMy4yMi41NDZBLjMuMyAwIDAwMjMuMDQ4IDBIMS41QTEuNSAxLjUgMCAwMDAgMS41djEzLjg2YS43NS43NSAwIDAwMS4xOC42MTR6IiBmaWxsPSIjMDAwIi8+PHBhdGggZD0iTTkuMjA1IDEwLjI4OWEzLjM3NSAzLjM3NSAwIDAxNS40NjEtMy45NjgiIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyIvPjwvZz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Invert Colors
                                </div>
                                <div class="text">Full color inversion for all site elements providing greater perceived color depth, making texts significantly easier to read while reducing eye strain for color blind and visually impaired users.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjEuNSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48Y2lyY2xlIGN4PSIxMiIgY3k9IjEyIiByPSIxMiIvPjxwYXRoIGQ9Ik0xMiAwQzUuMzczIDAgMCA1LjM3MyAwIDEyczUuMzczIDEyIDEyIDEyaDAiIGZpbGw9IiMwMDAiLz48L2c+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Color Desaturation
                                </div>
                                <div class="text">Desaturates all colors on the page. An important accessibility function that helps users with deuteranopia, tritanopia, and other forms of color blindness to better distinguish your site’s content.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMCAwaDI1LjQ5MnYyNS42NzRIMHoiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyLjEyNCAyLjg1MykiIGZpbGw9IiMwMDAiPjxwYXRoIGQ9Ik01LjQwOCAxNi45NThsLTQuMiA0LjIyOGMtLjQ0NS40NS0xLjIwOC4xMzItMS4yMDgtLjUwNFYyLjkzMkEyLjkyMiAyLjkyMiAwIDAxMi45MTEgMGgxNS40MjFhMi45MjIgMi45MjIgMCAwMTIuOTEyIDIuOTMydjExLjA5NGEyLjkyMiAyLjkyMiAwIDAxLTIuOTEyIDIuOTMySDUuNDA4ek0xLjQxNiAxOC45NmwzLjE5Ny0zLjIyYS43MDYuNzA2IDAgMDEuNTAxLS4yMDloMTMuMjE4YTEuNSAxLjUgMCAwMDEuNDk1LTEuNTA1VjIuOTMyYTEuNSAxLjUgMCAwMC0xLjQ5NS0xLjUwNkgyLjkxMmExLjUgMS41IDAgMDAtMS40OTYgMS41MDZWMTguOTZ6IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNOS45MTQgNy4wMjR2NS40OWEuNzEuNzEgMCAwMC43MDguNzEyLjcxLjcxIDAgMDAuNzA4LS43MTN2LTUuNDlhLjcxLjcxIDAgMDAtLjcwOC0uNzEyLjcxLjcxIDAgMDAtLjcwOC43MTN6IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48Y2lyY2xlIGN4PSIxMC42MjIiIGN5PSI0LjI3OSIgcj0iMSIvPjwvZz48L2c+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Tooltips
                                </div>
                                <div class="text">Shows alternative text and aria labels for on-screen elements with a simple mouse hover. Tooltips are prominent, high contrast, and easy to read for low-vision users with accessibility needs.</div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjgiIGhlaWdodD0iMjAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMjIuMjMzLjY3NUwzLjQ5OC43MTNhLjgyMi44MjIgMCAwMC0uODIzLjgyNVYxOC41YS44MjIuODIyIDAgMDAuODI1LjgyNWgyMWEuODIyLjgyMiAwIDAwLjgyNS0uODI1VjEuNTI1QS44MjIuODIyIDAgMDAyNC41MDkuN0wyMi4yMzMuNjc1eiIgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjEuMzUiLz48cGF0aCBkPSJNLjc1IDdoMjYuNWEuNzUuNzUgMCAwMS43NS43NXY0LjVhLjc1Ljc1IDAgMDEtLjc1Ljc1SC43NWEuNzUuNzUgMCAwMS0uNzUtLjc1di00LjVBLjc1Ljc1IDAgMDEuNzUgN3oiIGZpbGw9IiMwMDAiLz48cmVjdCBmaWxsPSIjRkZGIiB4PSIyIiB5PSI5IiB3aWR0aD0iMjQiIGhlaWdodD0iMiIgcng9Ii4xODgiLz48L2c+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Reading Guide
                                </div>
                                <div class="text">Converts the cursor into a high contrast horizontal reading guide. Focuses and guides the eyes to reduce eye strain for users who are visually impaired, dyslexic, cognitively disabled, and users with Presbyopia.</div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMTgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0Ljc5MyAxNy4yNWwtMS43My0zLjg4OEgzLjQ1NWwtMS43MyAzLjg4OEguMDE4TDcuNDA2Ljc2MmgxLjcwNkwxNi41IDE3LjI1aC0xLjcwN3pNNC4xNCAxMS44NzRoOC4yNEw4LjI2IDIuNjU4bC00LjEyIDkuMjE2em0yMS4zODItNy4zMmMzLjI0MiAwIDUuNzU0IDIuNzg0IDUuNzU0IDYuNDggMCAzLjcyLTIuNTEyIDYuNTA0LTUuNzU0IDYuNTA0LTIuMDczIDAtMy43My0xLjAzMi00LjcwNi0yLjcxMnYyLjQyNGgtMS41MTFWLjQ3NGgxLjUxMXY2Ljc5MmMuOTc1LTEuNjggMi42MzMtMi43MTIgNC43MDYtMi43MTJ6bS0uMjY4IDExLjYxNmMyLjUzNSAwIDQuNDYxLTIuMjA4IDQuNDYxLTUuMTM2cy0xLjkyNi01LjExMi00LjQ2MS01LjExMmMtMi42MSAwLTQuNDM4IDIuMTEyLTQuNDM4IDUuMTEyczEuODI5IDUuMTM2IDQuNDM4IDUuMTM2eiIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9zdmc+"
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Accessible Fonts
                                </div>
                                <div class="text">Converts stylized fonts into accessible fonts and font weights that are essential for visually impaired and dyslexic users who find nonstandard fonts confusing or difficult to read.</div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjMiIGhlaWdodD0iMTciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTAgMTdWLjc4aDUuNTUyYzUuMjE4IDAgOC45MDMgMy4zNTIgOC45MDMgOC4wOThTMTAuNzcgMTcgNS41NTIgMTdIMHptMS44NjctMy4zM2gzLjY4NWM0LjExNyAwIDcuMDEzLTIuMjEyIDcuMDEzLTUuNjE4IDAtMy4zODUtMi44OTYtNS41OTYtNy4wMTMtNS41OTZIMS44NjdWMTMuNjd6TTE5Ljc2IDMuMTE3bC4wMDkgMS42NzZoMy4wNzZ2MS41ODJoLTMuMDY4TDIwLjE0IDE3aC0yLjcxbC40MTgtMTAuNjI1SDE1Ljk5VjQuNzkzaDEuOTYybC4wNTctMS42NzZDMTguMDcxIDEuMjk5IDE5LjI0NCAwIDIwLjg1MiAwYy43NiAwIDEuNTE4LjI4MyAyLjE0OC43OGwtLjczOCAxLjI5OGMtLjI2Ny0uMjg0LS43My0uNDk2LTEuMjQ4LS40OTYtLjY3NiAwLTEuMjU5LjYzNy0xLjI1NCAxLjUzNXoiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg=="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Dyslexia Friendly Font
                                </div>
                                <div class="text">Dyslexia Friendly Font enhances readability for those with dyslexia and provides an easier and more fluid reading experience that is tailored to site visitors with dyslexia.</div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="feature-block bootdey" style="visibility: visible;">
                                <div class="ico">
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjEuMzUiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCI+PHBhdGggZD0iTTExLjk4OSAxMy43NEwuNjc2IDcuMjA3IDExLjk4OS42NzVsMTEuMzEzIDYuNTMyeiIvPjxwYXRoIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgZD0iTTIzLjMwMiAxMS43NEwxMS45OSAxOC4yNzIuNjc1IDExLjc0Ii8+PHBhdGggc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBkPSJNMjMuMzAyIDE2LjY3NUwxMS45OSAyMy4yMDguNjc1IDE2LjY3NSIvPjwvZz48L3N2Zz4="
                                        alt=""
                                        aria-hidden="true"
                                    />
                                </div>
                                <div class="name">
                                    Page Structure
                                </div>
                                <div class="text">Quickly reveal page headings, landmarks and links in a clear, structured, easily navigable and accessible manner. Helps both disabled and non-disabled users find the content they want, faster.</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <hr>
                <br>
                <h2>How to use EmVision</h2>
                <p>Watch this demonstration video.</p>
                <iframe width="854" height="480"
                    src="https://www.youtube.com/embed/AuNWbef39XE">
                </iframe>
            </div>
        </div>
    </div>
</section>
</div>
</div>
</div>
</div>                                        
<style>

                        
h2 {
    font-size: 50px;
    font-weight: 300;
    margin-bottom: 10px;
    line-height: 50px;
}
.highlight {
    color: #2ac5ed;
}
#content {
    padding: 70px 0;
}
#content .features-list {
    padding-top: 35px;
}
.features-list .feature-block {
    margin-bottom: 18px;
}
.features-list .feature-block .ico {
    font-size: 37px;
    line-height: 70px;
    width:70px;
    height: 70px;
    background: #5accff;
    display: inline-block;
    border-radius: 50%;
    color: #FFFFFF;
    margin-bottom: 10px;
}
.features-list .feature-block.bottom-line .ico {
    width: auto;
    height: auto;
	background: transparent;
	color: #5accff;
	text-align: center;
	font-size: 41px;
	vertical-align: top;
	margin-top: -10px;
}
.features-list .feature-block.bottom-line .fa-github {
	font-size: 50px;
}
.features-list .feature-block.bottom-line .fa-dashboard {
	font-size: 45px;
	margin-top: -15px;
}
.features-list .feature-block.bottom-line .ico {
	float: left;
	margin-right: 15px;
	margin-left: 21px;
}
.features-list .feature-block.bottom-line .features-content {
	padding-right: 15px;
	display: table;
}
.features-list .feature-block.bottom-line .features-content .name {
	margin-bottom: 5px;
}
.features-list .feature-block.bottom-line .features-content .subname {
	font-size: 16px;
	margin-bottom: 12px;
}
.features-list .feature-block .name {
    font-size: 20px;
    line-height: 1.25em;
    font-weight: bold;
    margin-bottom: 10px;
}
.features-list .feature-block .text {
    font-size: 12px;
    line-height: normal;
    margin-bottom: 15px;
}
                                        
</style>
@endsection
