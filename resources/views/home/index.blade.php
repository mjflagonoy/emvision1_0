@extends('layouts.app')

@section('content')
 <!-- Additional CSS Files -->
 <link rel="stylesheet" type="text/css" href="assets/css/templatemo-art-factory.css">
 <link rel="stylesheet" type="text/css" href="assets/css/owl-carousel.css">
    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome">

      <!-- ***** Header Text Start ***** -->
      <div class="header-text">
          <div class="container" style="padding-top: 1.5em">
              <div class="row">
                  <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                      <p style="font-size: 2em; line-height: 4rem; font-weight: bold" >Expanding Employment Possibilities for People with Vision Loss</p>
                      <p>Welcome to EmVision, an employment information resource for job seekers who are blind or visually impaired. EmVision provides employment information, career exploration tools, and job seeking guidance for individuals with vision loss and the professionals who work with them.</p>
                      <a href="{{route('/manual')}}" class="main-button-slider">How to Use EmVision</a>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt-5" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                      <img src="https://i.imgur.com/hoJO14R.png" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic">
                  </div>
              </div>
          </div>
      </div>
      <!-- ***** Header Text End ***** -->
  </div>
  <!-- ***** Welcome Area End ***** -->


  <!-- ***** Features Big Item Start ***** -->
  <section class="section" id="about">
      <div class="container">
          <div class="row">
              <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                  <img src="https://i.imgur.com/tLLe0W8.png" class="rounded img-fluid d-block mx-auto" alt="App">
              </div>
              <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                  <div class="left-heading">
                      <p style="font-weight: bold">Start Your Career Today</p>
                  </div>
                  <div class="left-text">
                        <p>Are you a job seeker who is blind or visually impaired? EmVision specifically highlights job opportunities for jobseekers with visual impairments. This collaboration between willing companies is a free service, with a goal of connecting you with accessible career opportunities.</p>
                        <a href="{{ route('jobs') }}" class="main-button">Start your Job Search</a>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-lg-12">
                  <div class="hr"></div>
              </div>
          </div>
      </div>
  </section>
  <!-- ***** Features Big Item End ***** -->


  <!-- ***** Features Big Item Start ***** -->
  <section class="section" id="about2">
      <div class="container">
          <div class="row">
              <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                  <div class="left-heading">
                      <p style="font-weight: bold">Build Your Workforce</p>
                  </div>
                  <p>If you are an employer willing to give chance to people with visual impairment, we invite you to post your open positions. We’re ready to connect you with your next great employee.</p>
                  <a href="{{ url('company/add-job') }}" class="main-button">Post your jobs</a>
              </div>
              <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                  <img src="https://i.imgur.com/YRT7UQQ.png" class="rounded img-fluid d-block mx-auto" alt="App">
              </div>
          </div>
      </div>
  </section>
  <!-- ***** Features Big Item End ***** -->


  <!-- ***** Features Small Start ***** -->
  <section class="section" id="services">
      <div class="container">
          <div class="row">
            <div style="width:100%; display: flex; justify-content: center;">
                <p style="color: #fff; font-weight: bold; font-size: 2em; margin-bottom: 10px">Recent Companies</p>
            </div>
              <div class="owl-carousel owl-theme">
                  @foreach ($companies as $company)
                        <div class="item service-item">
                            <div class="icon">
                                <i><img src="https://i.imgur.com/A71C6q3.png" alt=""></i>
                            </div>
                            <h4 class="service-title" style="font-weight: bold">{{ $company->name }}</h4>
                            <p>{{ $company->website }}</p>
                            <a href="{{ $company->username }}" class="main-button">Read More</a>
                        </div>
                        
                  @endforeach
                  
                  
              </div>
          </div>
      </div>
  </section>
  <!-- ***** Features Small End ***** -->

          </div>
      </div>
  </section>
  <!-- ***** Contact Us End ***** -->

  
  <!-- ***** Footer Start ***** -->
  <footer>
      <div class="container">
          <div class="row">
              <div class="col-lg-7 col-md-12 col-sm-12">
                  <p class="copyright">Copyright &copy; 2021 EmVision</p>
              </div>
              {{-- <div class="col-lg-5 col-md-12 col-sm-12">
                  <ul class="social">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="#"><i class="fa fa-rss"></i></a></li>
                      <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                  </ul>
              </div> --}}
          </div>
      </div>
  </footer>
  
  <!-- jQuery -->
  <script src="assets/js/jquery-2.1.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/popper.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Plugins -->
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/scrollreveal.min.js"></script>
  <script src="assets/js/waypoints.min.js"></script>
  <script src="assets/js/jquery.counterup.min.js"></script>
  <script src="assets/js/imgfix.min.js"></script> 
  
  <!-- Global Init -->
  <script src="assets/js/custom.js"></script>
@endsection