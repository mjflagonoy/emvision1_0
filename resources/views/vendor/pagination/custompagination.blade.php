@if ($paginator->hasPages())
     
                    <nav aria-label="Page navigation">
                        <ul class="pagination pagination-reset justify-content-center">
                            @if ($paginator->onFirstPage())
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"  rel="prev">
                                        <i class="zmdi zmdi-long-arrow-left"></i>
                                    </a>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" tabindex="-1" aria-disabled="false">
                                        <i class="zmdi zmdi-long-arrow-left"></i>
                                    </a>
                                </li>
                                
                            @endif


                            @foreach ($elements as $element)
           
                                @if (is_string($element))
                                    <li class="page-item disabled"><a class="page-link" href="#">{{ $element }}</a></li>
                                @endif

                                @if (is_array($element))
                                    @foreach ($element as $page => $url)
                                        @if ($page == $paginator->currentPage())
                                           
                                            <li class="page-item d-none d-md-inline-block active"><a class="page-link" href="#">{{ $page }}</a></li>
                                        @else
                                            <li class="page-item d-none d-md-inline-block"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                            
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach

                            @if ($paginator->hasMorePages())
                                <li class="page-item">
                                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}"  rel="next">
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </a>
                                </li>
                            @else
                                <li class="page-item disabled">
                                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}"  rel="next">
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                   
@endif
