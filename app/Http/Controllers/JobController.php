<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(){
        $posts = Post::where('status', '=', 1)->paginate(5);
        $cats=Category::all();
        return view('jobs', compact('posts'), compact('cats'));
    }

    public function autocompleteSearch(Request $request)
    {
        $query = $request->get('query');
        $filterResult = Post::where('title', 'LIKE', '%'. $query. '%')->get();
        return response()->json($filterResult);
    } 

    public function search(Request $request){
        $posts = Post::where('status', '=', 1);

        if($request->searchKeyword !== null){
            $posts = $posts->where('title','LIKE','%'.$request->searchKeyword.'%')
                ->orWhere('tags','LIKE','%'.$request->searchKeyword.'%');
        } 

        if($request->searchCategory !== null){
            $posts = $posts->where('cat_id', '=', $request->searchCategory);
        }

        if($request->salaryMax !== null){
            $posts = $posts->where('salaryAvg', '<=', $request->salaryMax);
        }

        if($request->salaryMin !== null){
            $posts = $posts->where('salaryAvg', '>=', $request->salaryMin);
        }

        $posts = $posts->paginate(10);
   
        $cats=Category::all();
        return view('jobs', compact('posts'), compact('cats'));
    }

    public function searchUsers(Request $request){
        $applicants = User::where('name', 'LIKE', '%'.$request->searchUser.'%')
                        ->where('role', '=', 'applicant')
                        ->paginate(10);
        
        
        $companies = User::where('name', 'LIKE', '%'.$request->searchUser.'%')
                        ->where('role', '=', 'employer')
                        ->paginate(10);

        return view('searchUsers', compact('applicants'), compact('companies'))->with('query', $request->searchUser);
    }

    public function getCompanies(){
        $companies = Rating::getTopCompanies();
        return view('companies', compact('companies'));
    }
   
}
