<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicantController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:applicant']);
    }

    public function apply(Request $request, $slug, $postId){
        $detail=Post::find($postId);
        $job_application = JobApplication::where([['post_id', '=', $postId], ['applicant_id', '=', auth()->user()->id]])->first();
        
        if($detail->status === 1){
            if($job_application !== null){
                return redirect('detail/'.$slug.'/'.$postId)->with('error', "You have already applied to this job.");
            }
            else return view('post.applyNow', ['detail' => $detail]);
        }
        return redirect('detail/'.$slug.'/'.$postId)->with('error', "This job post is closed");
    }

    public function submitApplication(Request $request, $postId){
        $post = Post::find($postId);
        $job_application = JobApplication::where([['post_id', '=', $postId], ['applicant_id', '=', auth()->user()->id]])->first();
     
        //Job application Status
        // Received (Green)
        // Reviewed (Green)
        // Scheduled for Interview (Green)
        // Not suitable (Secondary)
        // Void (Secondary)

        if($post->status === 1){
            if($job_application !== null){
                return redirect('detail/'.$post->title.'/'.$postId)->with('error', "You have already applied to this job.");
            }
            else{
                $application = new JobApplication();
                $application->post_id = $postId;
                $application->company_id = $post->user->id;
                $application->applicant_id = auth()->user()->id;
                $application->application_status = "Received";
                $application->post_status = $post->status;
                $application->applicant_reason = $request->applicantReason;

                if($request->file()) {
                    $fileName = time().'_'.$request->file->getClientOriginalName();
                    $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');

                    $application->resume_name = time().'_'.$request->file->getClientOriginalName();
                    $application->resume_path = $filePath;
                }

                $application->save();

                return redirect('dashboard');
            }
        }
        return redirect('detail/'.$post->title.'/'.$postId)->with('error', "This job post is closed");

    }

    

    public function rateCompany(Request $request, $username, $rating){
        $user = DB::table('users')->where('username', $username)->first();
        $prev_rating = Rating::where([['company_id', '=', $user->id], ['applicant_id', '=', auth()->user()->id]])->first();
        
        if(isset($prev_rating) or $rating>5 or $rating<1){
            return back();
        }
        else{
            $nrating = new Rating();
            $nrating->company_id = $user->id;
            $nrating->applicant_id = auth()->user()->id;
            $nrating->rating = $rating;
            $nrating->save();
        }
        

        return back();
    }
}
