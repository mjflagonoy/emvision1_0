<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(){
        if(auth()->user()->role === "employer"){
            $posts = Post::where([['userId', auth()->user()->id], ['status', '=', 1]])->paginate(5);
            $inactive = Post::where([['userId', auth()->user()->id], ['status', '=', 0]])->paginate(5);
            
            return view('dashboard.companyDashboard', compact('posts'), compact('inactive'));
        }
        elseif(auth()->user()->role === "applicant"){
            $posts = JobApplication::where([['applicant_id', '=', auth()->user()->id], ['post_status', '=', 1]])->paginate(5);
            $inactive = JobApplication::where([['applicant_id', '=', auth()->user()->id], ['post_status', '=', 0]])->paginate(5);
            
            return view('dashboard.applicantDashboard', compact('posts'), compact('inactive'));
        }
        else{
            $posts = Post::paginate(10);
            $companies = User::where([['role', '=', 'employer']])->paginate(10);
            $applicants = User::where([['role', '=', 'applicant']])->paginate(10);
            $admins = User::where([['role', '=', 'admin']])->paginate(10);
            return view('admin.adminDashboard', compact('posts'), compact('companies'))->with('data', ['applicants' => $applicants, 'admins' => $admins]);
        }
    }
}
