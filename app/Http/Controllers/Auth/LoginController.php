<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index(){
        return view('auth.login');
    }

    public function store(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if(!auth()->attempt($request->only('email', 'password'), $request->rememberme)){
            return back()->with('status', 'Invalid login details');
        }

        if(auth()->user()->status === 1){
            return redirect()->route('dashboard');
        }
        else{
            Auth::logout();
            return view('deactivated');
        }
        
    }
}
