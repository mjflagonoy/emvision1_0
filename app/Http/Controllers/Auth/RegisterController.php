<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session as FacadesSession;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {
        FacadesSession::put('role', 'applicant');
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $ROLE_APPLICANT = 'applicant';

        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $ROLE_APPLICANT,
            'status' => 1,
        ]);

        $user->markEmailAsVerified();

        
        auth()->attempt($request->only('email', 'password'));

        return redirect()->route('dashboard');
    }

    
}
