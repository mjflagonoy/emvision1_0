<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CompanyProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:employer']);
    }

    public function saveCompanyInformation(Request $request){
        $request->validate([
            'inputCompanyname' => 'required',
            'inputEmail' => 'required',
        ]);

        User::where('id', auth()->user()->id)
                ->update(['name' => $request->inputCompanyname,
                        'email' => $request->inputEmail,
                        'website' => $request->inputWebsite,
                        'company_industry' => $request->inputIndustry,
                        'company_size' => $request->inputCompanySize,
                        'company_type' => $request->inputCompanyType,
                        'company_specialties' => $request->inputCompanySpecialties,
                        'company_locations' => $request->inputLocations
            ]);

        return back();
    }

    
}
