<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    
    public function index($username)
    {
        
        $user = DB::table('users')->where('username', $username)->first();

        if(!is_null($user)){
            $userProfile = DB::table('users')->where('username', $username)->first();
            if( strcmp($user->role, 'applicant') == 0){
                $userSkills = DB::table('applicant_skill')->where('userId', '=', $userProfile->id)->get();
                $userEducation = DB::table('applicant_educational_background')->where('userId', '=', $userProfile->id)->orderBy('yearGraduation', 'desc')->get();
                $userExperience = DB::table('applicant_work_experience')->where('userId', '=', $userProfile->id)->orderBy('yearEnd', 'desc')->get();
                $userAward = DB::table('applicant_award')->where('userId', '=', $userProfile->id)->orderBy('awardYear', 'desc')->get();
                
                return view('profile.applicantProfile')->with('userProfile', ['user' => $userProfile, 
                                                                              'skills' => $userSkills,
                                                                              'educations' => $userEducation,
                                                                              'experiences' => $userExperience,
                                                                              'awards' => $userAward]);
            }
            elseif(strcmp($user->role, 'employer') == 0){
                $rating = Rating::where([['company_id', '=', $user->id], ['applicant_id', '=', auth()->user()->id]])->first();
                $posts = Post::where([['userId', '=', $user->id], ['status', '=', 1]])->paginate(5);
                return view('profile.companyProfile', compact('posts'))->with('userProfile', ['user' => $userProfile, 'rating' => $rating,
                                                                             'avgRating' => Rating::getAvgRating($user->id),
                                                                             'percent5' => Rating::getPercentRating($user->id, 5),
                                                                             'percent4' => Rating::getPercentRating($user->id, 4),
                                                                             'percent3' => Rating::getPercentRating($user->id, 3),
                                                                             'percent2' => Rating::getPercentRating($user->id, 2),
                                                                             'percent1' => Rating::getPercentRating($user->id, 1),
                                                                            ]);
            }
            else{
                if(auth()->user()->id === $user->id){
                    return view('admin.adminProfile');
                }
                else{
                    dd("profile does not exist");
                }
                
            }
        }
        else{
            dd("profile does not exist");
        }
    }

    public function showApplicantEdit(){
        $username = auth()->user()->username;
        $userProfile = DB::table('users')->where('username', $username)->first();
        $userSkills = DB::table('applicant_skill')->where('userId', '=', $userProfile->id)->get();
        $userEducation = DB::table('applicant_educational_background')->where('userId', '=', $userProfile->id)->orderBy('yearGraduation', 'desc')->get();
        $userExperience = DB::table('applicant_work_experience')->where('userId', '=', $userProfile->id)->orderBy('yearEnd', 'desc')->get();
        $userAward = DB::table('applicant_award')->where('userId', '=', $userProfile->id)->orderBy('awardYear', 'desc')->get();

        return view('profile.applicantEdit')->with('currentUser', [ 'user' => auth()->user(), 
                                                                    'skills' => $userSkills,
                                                                    'educations' => $userEducation,
                                                                    'experiences' => $userExperience,
                                                                    'awards' => $userAward]);
    }

    public function showCompanyEdit(){
        
        $username = auth()->user()->username;
        return view('profile.companyEdit')->with('currentUser', [ 'user' => auth()->user()]);
    }
}
