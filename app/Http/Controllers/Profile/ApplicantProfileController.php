<?php

namespace App\Http\Controllers\profile;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Ui\Presets\React;

class ApplicantProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:applicant']);
    }

    public function saveEducation(Request $request){

        $request->validate([
            'education.*.school' => 'required',
            'education.*.level' => 'required',
            'education.*.year' => 'required',
            
        ]);

        foreach ($request->education as $key => $value) {
            DB::table('applicant_educational_background')->updateOrInsert([
                'id' => $value['id'],
            ],
            [
                'userId' => auth()->user()->id,
                'yearGraduation' => $value['year'],
                'level' => $value['level'],
                'school' => $value['school'],
                'field' => $value['field']
            ]);
        }

        $this->saveWorkExperience($request);
        $this->saveSkill($request);
        $this->saveAward($request);

        return back()->withInput(['list'=>'vitae']);

    }

    public function deleteEducation($id){
        DB::table('applicant_educational_background')->where('id', $id)->delete();
        return back();
    }

    public function saveWorkExperience(Request $request){
        $request->validate([
            'work.*.company' => 'required',
            'work.*.position' => 'required',
            'work.*.location' => 'required',
            'work.*.yearEmployment' => 'required',
        ]);

        foreach ($request->work as $key => $value) {
            DB::table('applicant_work_experience')->updateOrInsert([
                'id' => $value['id'],
            ],
            [
                'userId' => auth()->user()->id,
                'company' => $value['company'],
                'position' => $value['position'],
                'location' => $value['location'],
                'yearEmployment' => $value['yearEmployment'],
                'yearEnd' => $value['yearEnd']
            ]);
        }
       
    }

    public function deleteExperience($id){
        DB::table('applicant_work_experience')->where('id', $id)->delete();
        return back();
    }

    public function deleteSkill($id){
        DB::table('applicant_skill')->where('id', $id)->delete();
        return back();
    }

    public function deleteAward($id){
        DB::table('applicant_award')->where('id', $id)->delete();
        return back();
    }

    public function saveAward(Request $request){
        $request->validate([
            'certificate.*.award' => 'required',
            'certificate.*.yearOfObtainment' => 'required',
        ]);

        foreach ($request->certificate as $key => $value) {
            DB::table('applicant_award')->updateOrInsert([
                'id' => $value['id'],
            ],
            [
                'userId' => auth()->user()->id,
                'award' => $value['award'],
                'awardYear' => $value['yearOfObtainment'],
                'certifyingBody' => $value['certifyingBody']
            ]);
        }
       
    }

    public function saveSkill(Request $request){
        $request->validate([
            'skill.*.skill' => 'required',
            'skill.*.proficiency' => 'required',
        ]);

        foreach ($request->skill as $key => $value) {
            DB::table('applicant_skill')->updateOrInsert([
                'id' => $value['id'],
            ],
            [
                'userId' => auth()->user()->id,
                'skill' => $value['skill'],
                'proficiency' => $value['proficiency']
            ]);
        }
        
    }

    public function saveInformation(Request $request){
        $request->validate([
            'inputFullname' => 'required',
            'inputEmail' => 'required',
        ]);

        User::where('id', auth()->user()->id)
                ->update(['name' => $request->inputFullname,
                        'biography' => $request->inputBio,
                        'email' => $request->inputEmail,
                        'number' => $request->inputNumber,
                        'address' => $request->inputAddress
            ]);

        return back();
    }

    public function deleteApplicant()
    {
       
        // $user = auth()->user();
        // DB::table('applicant_award')->where('userId', $user->id)->delete();
        // DB::table('applicant_skill')->where('userId', $user->id)->delete();
        // DB::table('applicant_work_experience')->where('userId', $user->id)->delete();
        // DB::table('applicant_educational_background')->where('userId', $user->id)->delete();
        // DB::table('users')->where('id', $user->id)->delete();
    
        // $status = 'Successfully Done';
        // return back()->with(['status' => $status]);

        DB::table('users')->where('id', auth()->user()->id)->update([
            'status' => 0,
        ]);
        Auth::logout();
        return redirect()->route('login');
    }

}
