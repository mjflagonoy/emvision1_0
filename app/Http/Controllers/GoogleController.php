<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laravel\Ui\Presets\React;

class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
        
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        
        $role = Session::get('role');
        Session::forget('role');
        if($role === null){
            $role = 'applicant';
        }

        try {
           
            $googleUser = Socialite::driver('google')->user();        
            $existUser = User::where('email', $googleUser->email)->first();
            

            if ($existUser) {
                Auth::loginUsingId($existUser->id);
                $role = $existUser->role;
            } else {
                $user = new User;
                $user->name = $googleUser->name;
                $user->email = $googleUser->email;
                $user->google_id = $googleUser->id;
                $user->password = md5(rand(1, 10000));
				$user->username = substr($googleUser->email , 0, strrpos($googleUser->email , '@'));
                $user->role = $role;
                $user->status = 1;
                $user->markEmailAsVerified();
                $user->save();
                Auth::loginUsingId($user->id);
            }


            if(auth()->user()->status === 1){
                return redirect()->route('dashboard');
            }
            else{
                Auth::logout();
                return view('deactivated');
            }


        } catch (Exception $e) {
            return $e;
        }


    }

    
}

//--------------------------------------------------------------------------------------------------------------