<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\Reply;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    

    function detail(Request $request,$slug,$postId){
        // Update post count
        Post::find($postId)->increment('views');
    	$detail=Post::find($postId);

        if(auth()->user()->role === 'applicant')
            $job_application = JobApplication::where([['post_id', '=', $postId], ['applicant_id', '=', auth()->user()->id]])->first();
    	else $job_application = JobApplication::where([['post_id', '=', $postId], ['company_id', '=', auth()->user()->id]])->paginate(10);

        // return view('post.jobdetail',['detail'=>$detail, 'job_application'=>$job_application]);
        
        return view('post.detail', compact('detail'), compact('job_application'));
    }

    function save_comment(Request $request,$slug,$id){
        $request->validate([
            'comment'=>'required'
        ]);

        if($request->isPublic === "on") $isPublic = 1;
        else $isPublic = 0;

        $data=new Comment();
        $data->user_id=$request->user()->id;
        $data->post_id=$id;
        $data->comment=$request->comment;
        $data->slug=$slug;
        $data->isPublic = $isPublic;
        $data->save();
        return redirect('detail/'.$slug.'/'.$id)->with('success','Comment has been submitted.');
    }

    public function save_reply(Request $request, $postId, $commentId){

        $request->validate([
            'reply' => 'required'
        ]);

        $data = new Reply();
        $data->user_id = auth()->user()->id;
        $data->post_id = $postId;
        $data->comment_id = $commentId;
        $data->reply = $request->reply;
        $data->save();
        
        return back()->with('success','Reply has been submitted.');
    }

   

}
