<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index(){
        return view('admin.adminDashboard');
    }

    public function addAdmin(){
        User::create([
            'username' => 'admin',
            'name' => 'Iam Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'role' => 'admin',
        ]);
    }

    public function editPost(Request $request, $slug, $id){
        $cats=Category::all();
        $post=Post::where([['id', '=', $id]])->first();
        return view('admin.editPost', ['cats'=>$cats, 'post'=>$post]);
    }

    public function updateAccountStatus(Request $request, $id, $status){
        DB::table('users')->where('id', $id)->update([
            'status' => $status,
        ]);

        
        if($status === '1') $str = "activated.";
        elseif($status === '0') $str = "deactivated.";
        return back()->with('success', 'Account has been '.$str);
    }

    public function addUser(Request $request)
    {
        

        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed',
            'role' => 'required'
        ]);

        User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,
            'status' => 1,
        ]);

      
        return back()->with('success', 'Account has been created');
    }

    public function editInfo(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed',
            'currentp' => 'required',
        ]);

        $user = User::find(auth()->user()->id);
        if(Hash::check($request->currentp, auth()->user()->password)){
            $user->name = $request->name;
            $user->email = $request->email;

            if($request->password !== null){
                $user->password = Hash::make($request->password);
            }
            
            $user->save();
        }
        else{
            return back()->with('success', 'Current password does not match.');
        }

        return back()->with('success', 'Account has been updated');
    }

}
