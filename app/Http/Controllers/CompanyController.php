<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\JobApplication;
use App\Models\Post;
use App\Models\Rating;
use App\Notifications\StatusUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:employer']);
    }

    public function index(){
        $companies = Rating::getTopCompanies();
        return view('companies', compact('companies'));
    }

    public function addJob(){
        $cats=Category::all();
        return view('post.add',['cats'=>$cats]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'category'=>'required',
            'detail'=>'required',
            'contract' => 'required',
            'position' => 'required',
            'workRegion' => 'required',
          
        ]);

      //  dd($request);
        if($request->displaySalary === 'on') $displaySalaryBool = 1;
        else $displaySalaryBool = 0;

        $post=new Post();
        $post->userId = auth()->user()->id;
        $post->cat_id = $request->category;
        $post->title = $request->title;
        $post->detail = $request->detail;
        $post->tags = $request->tags;
        $post->contract = $request->contract;
        $post->position = $request->position;
        $post->requiredExperience = $request->requiredExperience;
        $post->workRegion = $request->workRegionName;
        $post->workProvince = $request->workProvinceName;
        $post->workCity = $request->workCityName;
        $post->salaryMin = $request->salaryMin;
        $post->salaryMax = $request->salaryMax;
        $post->salaryAvg = ($request->salaryMin + $request->salaryMax)/2;
        $post->displaySalary = 1;
        $post->views = 0;
        $post->status = 1;
        //post status: active=1, stopped=0
        $post->save();

        
        return redirect('dashboard')->with('success','Data has been added');
    }

    public function updateApplicantStatus(Request $request,$slug,$postId){
        foreach ($request->applications as $key => $value) {
            if(isset($value['job_status'])){
                DB::table('job_applications')->where('id', $value['job_id'])->update([
                    'application_status' => $value['job_status']
                ]);
               
                $job_app = JobApplication::find($value['job_id']);

                $user = $job_app->applicant;
        
                Notification::send($user, new StatusUpdate());
            }   
        }
        
        
        return redirect('detail/'.$slug.'/'.$postId)->with('success','Each status has been updated');     
    }

    public function scheduleInterview(Request $request,$slug,$postId){

        DB::table('job_applications')->where('id', $request->job_id_modal)->update([
            'application_status' => "Scheduled for Interview",
            'interviewDate' => $request->interviewDate,
            'interviewTime' => $request->interviewTime,
            'interviewPlatform' => $request->interviewPlatform,
            'interviewNote' => $request->interviewNote
        ]);
            
        $job_app = JobApplication::find($request->job_id_modal);

        $user = $job_app->applicant;
        
        Notification::send($user, new StatusUpdate());

        return redirect('detail/'.$slug.'/'.$postId)->with('success','The schedule has been set');
    }

    public function downloadResume(Request $request, $jobApplicationId)
    {
        
        $jobApplication = JobApplication::find($jobApplicationId);
        $download = storage_path() . '\app\public\uploads\\'. $jobApplication->resume_name;
        return response()->download($download);
        
    }

    public function sendNotification($jobAppId, $newStatus)
    {
       // $user = User::first();
        $job_app = JobApplication::find($jobAppId);

        $user = $job_app->applicant;

        $details = [
            'greeting' => 'Hi Artisan',
            'body' => 'This is my first notification from ItSolutionStuff.com',
            'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
            'actionText' => 'View My Site',
            'actionURL' => url('/'),
            'order_id' => 101
        ];
  
        Notification::send($user, new StatusUpdate($details));
   
        dd('done');
    }

    public function updatePostStatus(Request $request,$slug,$postId, $status){
        DB::table('posts')->where('id', $postId)->update([
            'status' => $status,
        ]);

        DB::table('job_applications')->where('post_id', $postId)->update([
            'post_status' => $status,
        ]);

        if($status === '1') $str = "opened";
        else $str = "closed";

        return back()->with('success','This job post has been '.$str.' for application');
         
    }

    public function update(Request $request){
        $post = Post::find($request->id);
        $catid = $request->category;
        if($catid === null) $catid = $post->cat_id;

        $pos = $request->position;
        if($pos === null) $pos = $post->position;

        $post->cat_id = $catid;
        $post->title = $request->title;
        $post->detail = $request->detail;
        $post->tags = $request->tags;
        $post->contract = $request->contract;
        $post->position = $pos;
        $post->requiredExperience = $request->requiredExperience;
        $post->workRegion = $request->workRegionName;
        $post->workProvince = $request->workProvinceName;
        $post->workCity = $request->workCityName;
        $post->salaryMin = $request->salaryMin;
        $post->salaryMax = $request->salaryMax;
        $post->salaryAvg = ($request->salaryMin + $request->salaryMax)/2;
        $post->save();

        return back()->with('success', $post->title.' has been updated.');
    }
}
