<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $dates = ['interviewDate', 'interviewTime'];
    protected $table = 'job_applications';
    use HasFactory;
    protected $fillable = [
        'resume_name',
        'resume_path'
    ];
    function post(){
    	return $this->belongsTo('App\Models\Post', 'post_id');
    }

    function applicant(){
        return $this->belongsTo('App\Models\User', 'applicant_id');
    }
}
