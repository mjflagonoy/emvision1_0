<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';
    function comments(){
    	return $this->hasMany('App\Models\Comment')->orderBy('created_at','asc');
    }

    function category(){
    	return $this->belongsTo('App\Models\Category','cat_id');
    }

    function user(){
    	return $this->belongsTo('App\Models\User', 'userId');
    }

    public function getStatus(){
        $str = "";
        if($this->status === 1) $str = "Active";
        else $str = "Closed";
        return $str;
    }

    public function countApplicants(){
        return JobApplication::where('post_id', '=', $this->id)->count();
    }


}


