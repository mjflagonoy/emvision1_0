<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
		'google_id',
        'role',
        'address',
        'biography',
        'number',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        if($this->role === "admin")
        { 
            return true; 
        } 
        else 
        { 
            return false; 
        }
    }

    function getAvgRating(){
        return round(Rating::where([['company_id', '=', $this->id]])->avg('rating'),2);
    }
   
    function ratings(){
    	return $this->hasMany('App\Models\Rating', 'company_id')->orderBy('created_at','asc');
    }
}
