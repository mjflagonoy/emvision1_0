<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rating extends Model
{
    use HasFactory;
    protected $table = 'ratings';
    function applicant(){
        return $this->belongsTo('App\Models\User', 'applicant_id');
    }

    function company(){
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    static function getAvgRating($id){
        return round(Rating::where([['company_id', '=', $id]])->avg('rating'),2);
    }

    static function getPercentRating($id, $stars){
        $allRatings = Rating::where([['company_id', '=', $id]]);
        $certainRatings = Rating::where([['company_id', '=', $id]])->where([['rating', '=', $stars]]);
    
        if($allRatings->sum('rating') === 0) return 0;
        else $percent = ($certainRatings->count()*100)/$allRatings->count();
        
        return round($percent,0);
    }

    static function getTopCompanies(){
        $companies = User::where([['role', '=', 'employer']])->paginate(6);
        return $companies;
    }
}
