<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';

    function user(){
    	return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function replies()
    {
        return $this->hasMany('App\Models\Reply')->orderBy('created_at','asc');
    }

    public function isPublic(){
        if($this->isPublic === 1) return true;
        else return false;
    }
}
