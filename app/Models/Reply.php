<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory;
    protected $table = 'replies';
    function comment(){
    	return $this->belongsTo('App\Models\Comment');
    }
    // public function replies()
    // {
    //     return $this->hasMany(Reply::class, 'commentId');
    // }
    function user(){
    	return $this->belongsTo('App\Models\User');
    }
}
