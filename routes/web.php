<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\EmployerSignupController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ApplicantController;
use App\Http\Controllers\Profile\ProfileController;
use App\Http\Controllers\Profile\ApplicantProfileController;
use App\Http\Controllers\Profile\CompanyProfileController;
use App\Models\Company;
use App\Models\Rating;
use App\Models\User;

Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::post('/register', [RegisterController::class, 'store']);
Route::get('/register', [RegisterController::class, 'index'])->name('register');

Route::post('/login', [LoginController::class, 'store']);
Route::get('/login', [LoginController::class, 'index'])->name('login');

Route::get('/', function () {
    $companies = Rating::getTopCompanies();
    return view('home.index', compact('companies'));
})->name('home');

//manual
Route::get('/manual', function () {
    return view('manual');
})->name('/manual');

//Employer Page Routes
Route::post('/employer_signup', [EmployerSignupController::class, 'store']);
Route::get('/employer_signup', [EmployerSignupController::class, 'index'])->name('employer_signup');

//Job Seeker Page Routes
Route::get('/companies', [JobController::class, 'getCompanies'])->name('companies');
Route::get('/jobs', [JobController::class, 'index'])->name('jobs');

//Google Sign In Routes
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

//Applicant Profile routes
//Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
Route::get('{username}', [ProfileController::class, 'index']);
Route::get('/profile/edit', [ProfileController::class, 'showApplicantEdit'])->name('profile/edit');


Route::get('/profile/save-information', [ApplicantProfileController::class, 'saveInformation']);
Route::post('/profile/save-information', [ApplicantProfileController::class, 'saveInformation'])->name('profile/save-information');

Route::get('/profile/save-education', [ApplicantProfileController::class, 'saveEducation']);
Route::post('/profile/save-education', [ApplicantProfileController::class, 'saveEducation'])->name('profile/save-education');

Route::get('/profile/delete-education/{id}', [ApplicantProfileController::class, 'deleteEducation'])->name('profile/delete-education/{id}');
Route::get('/profile/delete-experience/{id}', [ApplicantProfileController::class, 'deleteExperience'])->name('profile/delete-education/{id}');
Route::get('/profile/delete-skill/{id}', [ApplicantProfileController::class, 'deleteSkill'])->name('profile/delete-skill/{id}');
Route::get('/profile/delete-award/{id}', [ApplicantProfileController::class, 'deleteAward'])->name('profile/delete-award/{id}');

Route::get('/profile/save-work-experience', [ApplicantProfileController::class, 'saveWorkExperience']);
Route::post('/profile/save-work-experience', [ApplicantProfileController::class, 'saveWorkExperience'])->name('profile/save-work-experience');

Route::get('/profile/save-award', [ApplicantProfileController::class, 'saveAward']);
Route::post('/profile/save-award', [ApplicantProfileController::class, 'saveAward'])->name('profile/save-award');

Route::get('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill']);
Route::post('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill'])->name('profile/save-skill');

Route::get('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill']);
Route::post('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill'])->name('profile/save-skill');

Route::get('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill']);
Route::post('/profile/save-skill', [ApplicantProfileController::class, 'saveSkill'])->name('profile/save-skill');

Route::get('/profile/delete-applicant', [ApplicantProfileController::class, 'deleteApplicant']);
Route::post('/profile/delete-applicant', [ApplicantProfileController::class, 'deleteApplicant'])->name('profile/delete-applicant');


//Applicant Routes

Route::get('jobs/apply/{slug}/{postId}', [ApplicantController::class, 'apply']);
Route::post('jobs/apply/{postId}/submit_application', [ApplicantController::class, 'submitApplication']);
Route::get('jobs/apply/{postId}/submit_application', [ApplicantController::class, 'submitApplication']);
Route::get('{username}/rate/{rating}', [ApplicantController::class, 'rateCompany']);

//Company Profile ROutes
Route::get('/profile/edit-company', [ProfileController::class, 'showCompanyEdit'])->name('profile/edit-company');

Route::get('/profile/save-company-information', [CompanyProfileController::class, 'saveCompanyInformation']);
Route::post('/profile/save-company-information', [CompanyProfileController::class, 'saveCompanyInformation'])->name('profile/save-company-information');

//Company -----------------------------------------
Route::get('/company/add-job', [CompanyController::class, 'addJob'])->name('company/add-job');

Route::get('/company/save-post', [CompanyController::class, 'store'])->name('company/save-post');
Route::post('/company/save-post', [CompanyController::class, 'store'])->name('company/save-post');
Route::post('/company/update-post', [CompanyController::class, 'update'])->name('company/update-post');

Route::post('/detail/{slug}/{id}/update-applicant-status',[CompanyController::class,'updateApplicantStatus']);
Route::post('/detail/{slug}/{id}/schedule-interview',[CompanyController::class,'scheduleInterview']);

Route::get('/detail/{slug}/{id}/update-status/{status}',[CompanyController::class,'updatePostStatus']);
Route::get('/download/{jobApplicationId}', [CompanyController::class, 'downloadResume']);
// Route::get('send/{jobAppId}', [CompanyController::class, 'sendNotification']);

//Posts Routes
Route::get('/detail/{slug}/{id}',[PostController::class,'detail']);
Route::post('/save-comment/{slug}/{id}',[PostController::class,'save_comment']);
Route::post('/save-reply/{postId}/{commentId}',[PostController::class,'save_reply']);

Route::get('auto-search', [JobController::class, 'autocompleteSearch']);
Route::get('/jobs/search', [JobController::class, 'search']);

Route::get('/users/search', [JobController::class, 'searchUsers']);

//Admin
Route::get('/detail/{slug}/{id}/edit',[AdminController::class,'editPost']);
Route::get('/account/update-status/{id}/{status}', [AdminController::class, 'updateAccountStatus']);
Route::post('/admin/add-user', [AdminController::class, 'addUser'])->name('/admin/add-user');
Route::post('/admin/edit-info', [AdminController::class, 'editInfo'])->name('/admin/edit-info');

