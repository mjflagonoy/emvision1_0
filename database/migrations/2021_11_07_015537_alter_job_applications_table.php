<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_applications', function (Blueprint $table) {
           $table->date('interviewDate')->nullable();
           $table->time('interviewTime')->nullable();
           $table->string('interviewPlatform')->nullable();
           $table->text('interviewNote')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_applications', function (Blueprint $table) {
            $table->dropColumn('interviewDate');
            $table->dropColumn('interviewTime');
            $table->dropColumn('interviewPlatform');
            $table->dropColumn('interviewNote');
         });
    }
}
