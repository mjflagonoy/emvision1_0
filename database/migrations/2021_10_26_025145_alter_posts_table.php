<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('workLocation', 'workRegion');
            $table->string('workProvince')->nullable();
            $table->string('workCity')->nullable();
            $table->unsignedDecimal('salaryAvg')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('workRegion', 'workLocation');
            $table->dropColumn('workProvince');
            $table->dropColumn('workCity');
        });
    }
}
