<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Replies', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_idd');
            $table->integer('post_idd');
            $table->integer('comment_id');
            $table->string('reply');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Replies');
    }
}
