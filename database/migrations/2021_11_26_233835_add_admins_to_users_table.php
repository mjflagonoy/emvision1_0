<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class AddAdminsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::create([
            'name' => "Jobelle Lagonoy",
            'username' => "lagonoy123",
            'email' => 'mjflagonoy@tip.edu.ph',
            'password' => Hash::make('IAmAdmin123'),
            'role' => "admin",
            'status' => 1,
        ]);

        User::create([
            'name' => "Arvielyn Oliva",
            'username' => "arvielyn",
            'email' => 'maholiva@tip.edu.ph',
            'password' => Hash::make('IAmAdmin123'),
            'role' => "admin",
            'status' => 1,
        ]);

        User::create([
            'name' => "Biel Laurent Zim Castro",
            'username' => "biel",
            'email' => 'mblzcastro@tip.edu.ph',
            'password' => Hash::make('IAmAdmin123'),
            'role' => "admin",
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
