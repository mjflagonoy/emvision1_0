<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('post_id');
            $table->text('comment');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('commentable_id')->unsigned()->nullable();
            $table->string('commentable_type')->nullable();
            $table->timestamps();
            $table->string('comment_type')->nullable();
            $table->string('slug');
     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
