<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('userId');
            $table->integer('cat_id');
            $table->string('title');
            $table->string('thumb')->nullable();
            $table->string('full_img')->nullable();
            $table->text('detail');
            $table->string('tags')->nullable();
            $table->timestamps();
            $table->integer('views');
            $table->integer('status')->nullable();
            $table->string('contract');
            $table->string('position');
            $table->unsignedInteger('requiredExperience')->nullable();
            $table->string('workLocation');
            $table->unsignedDecimal('salaryMin')->nullable();
            $table->unsignedDecimal('salaryMax')->nullable();
            $table->unsignedTinyInteger('displaySalary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
