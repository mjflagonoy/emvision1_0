<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDataToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')->insert([     
            ['title' => 'Administration, business and management'],     
            ['title' => 'Alternative therapies'],
            ['title' => 'Animals, land and environment'],
            ['title' => 'Computing and ICT'],
            ['title' => 'Construction and building'],
            ['title' => 'Design, arts and crafts'],
            ['title' => 'Education and training'],
            ['title' => 'Engineering'],
            ['title' => 'Facilities and property services'],
            ['title' => 'Financial services'],
            ['title' => 'Garage services'],
            ['title' => 'Hairdressing and beauty'],
            ['title' => 'Healthcare'],
            ['title' => 'Heritage, culture and libraries'],
            ['title' => 'Hospitality, catering and tourism'],
            ['title' => 'Languages'],
            ['title' => 'Legal and court services'],
            ['title' => 'Manufacturing and production'],
            ['title' => 'Performing arts and media'],
            ['title' => 'Print and publishing, marketing and advertising'],
            ['title' => 'Retail and customer services'],
            ['title' => 'Science, mathematics and statistics'],
            ['title' => 'Security, uniformed and protective services'],
            ['title' => 'Social sciences and religion'],
            ['title' => 'Social work and caring services'],
            ['title' => 'Sport and leisure'],
            ['title' => 'Transport, distribution and logistics'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
