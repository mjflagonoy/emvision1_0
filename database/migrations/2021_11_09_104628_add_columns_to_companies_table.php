<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('website')->nullable();
            $table->string('company_industry')->nullable();
			$table->integer('company_size')->nullable();
			$table->string('company_type')->nullable();
			$table->string('company_specialties')->nullable();
			$table->string('company_locations')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('website');
            $table->dropColumn('company_industry');
			$table->dropColumn('company_size');
			$table->dropColumn('company_type');
			$table->dropColumn('company_specialties');
			$table->dropColumn('company_locations');
        });
    }
}
